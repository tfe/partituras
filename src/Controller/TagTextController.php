<?php

namespace App\Controller;

use App\Entity\TagText;
use App\Entity\Tag;
use App\Form\TagTextType;
use App\Repository\TagTextRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admintag_text")
 */
class TagTextController extends AbstractController
{
    /**
     * @Route("/", name="tag_text_index", methods={"GET"})
     */
    public function index(TagTextRepository $tagTextRepository): Response
    {
        return $this->render('tag_text/index.html.twig', [
            'tag_texts' => $tagTextRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{id}", name="tag_text_new", methods={"GET","POST"})
     */
    public function new(Request $request, $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Tag::class);
        $tags = $repo->findById($id);
        if(!$tags)
        {
            throw $this->createNotFoundException('Does not exist.');
        }

        $tagText = new TagText();
        $tagText->setTag($tags[0]);
        $form = $this->createForm(TagTextType::class, $tagText);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tagText);
            $entityManager->flush();

            return $this->redirectToRoute('tag_edit', [
                'id' => $tagText->getTag()->getId(),
            ]);
        }

        return $this->render('tag_text/new.html.twig', [
            'tag_text' => $tagText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tag_text_show", methods={"GET"})
     */
    public function show(TagText $tagText): Response
    {
        return $this->render('tag_text/show.html.twig', [
            'tag_text' => $tagText,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tag_text_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TagText $tagText): Response
    {
        $form = $this->createForm(TagTextType::class, $tagText);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tag_edit', [
                'id' => $tagText->getTag()->getId(),
            ]);
        }

        return $this->render('tag_text/edit.html.twig', [
            'tag_text' => $tagText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tag_text_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TagText $tagText): Response
    {
        $tag_id = $tagText->getTag()->getId();
        if ($this->isCsrfTokenValid('delete'.$tagText->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tagText);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tag_edit', [ "id" => $partitura_id]);
    }
}
