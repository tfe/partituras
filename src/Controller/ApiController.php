<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Vich\UploaderBundle\Handler\DownloadHandler;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Filter;
use App\Entity\Partitura;
use App\Entity\UserFav;
use App\Entity\User;
use App\Entity\Tag;
use App\Entity\Instrumento;
use App\Entity\Search;
use App\Form\PartituraType;
use App\Form\SearchType;
use App\Form\FilterType;
use App\Repository\PartituraRepository;
use App\Repository\TagRepository;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Comentario;
use App\Form\ComentarioUserType;


/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/list", name="api_front", methods={"GET","POST","OPTIONS"})
     */
    public function list(TranslatorInterface $translator, Request $request, UploaderHelper $helper)
    {
        $page = 1;
        $repo = $this->getDoctrine()->getRepository(Partitura::class);

        $filter = new Filter();
        $sort = $request->request->get('filter_col');
        $filter->setCol('createdAt');
        $filter->setSort('desc');
        $filter_form = $this->createForm(Filtertype::class, $filter, [
             'action' => $this->generateUrl('front_partitura'),
             'method' => 'GET',
        ]);
        $filter_form->handleRequest($request);

        $num_per_pages = 12;
        $partituras = $repo->findLatest($page, $num_per_pages, $filter);
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $json = [
            "baseUrl" => $baseUrl,
            'items' => []
        ];
        foreach($partituras as $partitura)
        {
            /* Minimal information for the list answer */
            $item = [
                "id"            => $partitura->getId(),
                "name"          => $partitura->getName(),
                "views"         => $partitura->getViews(),
                "mainPdf"       => $helper->asset($partitura, 'musescoreFileFile'),
                "preview"       => str_replace('.pdf','.jpg',$helper->asset($partitura, 'musescoreFileFile')),
                "author"        => [
                    'id' => $partitura->getAuthor()->getId(),
                    'name' => $partitura->getAuthor()->getName(),
                ],
                "type"          => [
                    'id' => $partitura->getType()->getId(),
                    'name' => $partitura->getType()->getName(),
                ],
            ];
            $json['items'][] = $item;
        }

        $response = new JsonResponse($json);
        return $response;
    }

    /**
     * @Route("/get/{name}", name="api_get", methods={"GET","POST","OPTIONS"})
     */
    public function getIndex(TranslatorInterface $translator, Request $request, UploaderHelper $helper, $name)
    {
        $page = 1;
        $repo = $this->getDoctrine()->getRepository(Partitura::class);

        $filter = new Filter();
        $sort = $request->request->get('filter_col');
        $filter->setCol('createdAt');
        $filter->setSort('desc');
        $filter_form = $this->createForm(Filtertype::class, $filter, [
             'action' => $this->generateUrl('front_partitura'),
             'method' => 'GET',
        ]);
        $filter_form->handleRequest($request);

        $num_per_pages = 12;
        $partitura = $repo->findOneByName($name);
        if(!$partitura)
        {
            $response = new JsonResponse(["error" => "Not found"]);
            return $response;
        }

        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        /* All information for the GET answer */
        $json = [
            "id"            => $partitura->getId(),
            "name"          => $partitura->getName(),
            "baseUrl"       => $baseUrl,
            "views"         => $partitura->getViews(),
            "text"          => $partitura->getText(),
            "Description"   => $partitura->getDescription(),
            "validationComment"  => $partitura->getValidationComment(),
            "credit"        => $partitura->getCredito(),
            "creditUrl"     => $partitura->getCreditoUrl(),
            "mainPdf"       => $helper->asset($partitura, 'musescoreFileFile'),
            "preview"       => str_replace('.pdf','.jpg',$helper->asset($partitura, 'musescoreFileFile')),
            "mp3s"          => [],
            "pdfs"          => [],
            "instruments"   => [],
            "tags"          => [],
            "author"        => [
                'id' => $partitura->getAuthor()->getId(),
                'name' => $partitura->getAuthor()->getName(),
            ],
            "type"          => [
                'id' => $partitura->getType()->getId(),
                'name' => $partitura->getType()->getName(),
            ],
        ];

        $instrumentsAdded = [];
        foreach($partitura->getMp3s() as $mp3)
        {
            if(!isset($instrumentsAdded[$mp3->getInstrumento()->getId()]))
            {
                $instrumentsAdded[$mp3->getInstrumento()->getId()]=1;
                $json['instruments'][$mp3->getInstrumento()->getId()] = [
                        "image" => $helper->asset($mp3->getInstrumento(), 'imageFile'),
                        "id" => $mp3->getInstrumento()->getId(),
                        "name" => $mp3->getInstrumento()->getName(),
                ];
            }
            $mp3Item = [
                'id' => $mp3->getId(),
                'name' => $mp3->getName(),
                "mp3" => $helper->asset($mp3, 'mp3File'),
                "instrumento" => $mp3->getInstrumento()->getId()
            ];
            $json['mp3s'][] = $mp3Item;
        }
        foreach($partitura->getPartituraPdfs() as $pdf)
        {
            $pdfItem = [
                'id' => $pdf->getId(),
                'name' => $pdf->getName(),
                "pdf" => $helper->asset($pdf, 'pdfFile'),
                "instrumento" => $mp3->getInstrumento()->getId()
            ];
            $json['pdfs'][] = $pdfItem;
        }
        foreach($partitura->getTags() as $tag)
        {
            $tagItem = [
                'id' => $tag->getId(),
                "image" => $helper->asset($tag, 'imageFile'),
                'name' => $tag->getName(),
            ];
            $json['tags'][] = $tagItem;
        }

        //dump($json);
        //dump($partitura);exit;

        $response = new JsonResponse($json);
        return $response;
    }

}
