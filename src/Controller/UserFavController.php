<?php

namespace App\Controller;

use App\Entity\UserFav;
use App\Form\UserFavType;
use App\Repository\UserFavRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/adminuserfav")
 */
class UserFavController extends AbstractController
{
    /**
     * @Route("/", name="user_fav_index", methods={"GET"})
     */
    public function index(UserFavRepository $userFavRepository): Response
    {
        return $this->render('user_fav/index.html.twig', [
            'user_favs' => $userFavRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_fav_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $userFav = new UserFav();
        $form = $this->createForm(UserFavType::class, $userFav);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userFav);
            $entityManager->flush();

            return $this->redirectToRoute('user_fav_index');
        }

        return $this->render('user_fav/new.html.twig', [
            'user_fav' => $userFav,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_fav_show", methods={"GET"})
     */
    public function show(UserFav $userFav): Response
    {
        return $this->render('user_fav/show.html.twig', [
            'user_fav' => $userFav,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_fav_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserFav $userFav): Response
    {
        $form = $this->createForm(UserFavType::class, $userFav);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_fav_index');
        }

        return $this->render('user_fav/edit.html.twig', [
            'user_fav' => $userFav,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_fav_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UserFav $userFav): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userFav->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userFav);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_fav_index');
    }
}
