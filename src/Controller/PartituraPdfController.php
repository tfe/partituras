<?php

namespace App\Controller;

use App\Entity\Partitura;
use App\Entity\PartituraPdf;
use App\Form\PartituraPdfType;
use App\Repository\PartituraPdfRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/partiturapdf")
 */
class PartituraPdfController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    /**
     * @Route("/", name="partitura_pdf_index", methods={"GET"})
     */
    public function index(PartituraPdfRepository $partituraPdfRepository): Response
    {
        return $this->render('partitura_pdf/index.html.twig', [
            'partitura_pdfs' => $partituraPdfRepository->findAll(),
        ]);
    }

    /**
     * @Route({"es": "/nuevo/{id}", "eus":"/berria/{id}", "fr":"/nouveau/{id}"}, name="partitura_pdf_new",methods={"GET","POST"})
     */
    public function new(Request $request, $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $partituras = $repo->findById($id);
        if(!$partituras)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partituras[0]->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        $partituraPdf = new PartituraPdf();
        $partituraPdf->setPartitura($partituras[0]);
        $form = $this->createForm(PartituraPdfType::class, $partituraPdf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($partituraPdf);
            $entityManager->flush();

            return $this->redirectToRoute('partitura_edit', [ 'id' => $partituraPdf->getPartitura()->getId() ]);
        }

        return $this->render('partitura_pdf/new.html.twig', [
            'partitura_pdf' => $partituraPdf,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route({"es": "/edites/{id}", "eus":"/editeus/{id}", "fr":"/editfr/{id}"}, name="partitura_pdf_edit",methods={"GET","POST"})
     */
    public function edit(Request $request, PartituraPdf $partituraPdf): Response
    {
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partituraPdf->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        $form = $this->createForm(PartituraPdfType::class, $partituraPdf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('partitura_edit', [ 'id' => $partituraPdf->getPartitura()->getId() ]);
        }

        return $this->render('partitura_pdf/edit.html.twig', [
            'partitura_pdf' => $partituraPdf,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partitura_pdf_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PartituraPdf $partituraPdf): Response
    {
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partituraPdf->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $partitura_id = $partituraPdf->getPartitura()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($partituraPdf);
        $entityManager->flush();

        return $this->redirectToRoute('partitura_edit', [ "id" => $partitura_id]);
    }
}
