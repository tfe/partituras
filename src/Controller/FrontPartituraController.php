<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Vich\UploaderBundle\Handler\DownloadHandler;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Filter;
use App\Entity\FollowerMastodon;
use App\Entity\Partitura;
use App\Entity\UserFav;
use App\Entity\User;
use App\Entity\Tag;
use App\Entity\Instrumento;
use App\Entity\UserList;
use App\Entity\Search;
use App\Form\PartituraType;
use App\Form\PartituraFirstType;
use App\Form\SearchType;
use App\Form\FilterType;
use App\Repository\PartituraRepository;
use App\Repository\TagRepository;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\Comentario;
use App\Form\ComentarioUserType;


/**
 * @Route("/")
 */
class FrontPartituraController extends AbstractController
{
    private $default_keywords = ['partiturak.eus', 'partiturak','partitions','partitura','music sheet','gratis','free','gratuit','pdf','mp3','music','musica','musika'];

    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route({"eus": "/eus.html", "es":"/fakees.es", "fr":"fakefr.html"}, name="fake_partitura")
     */
    public function oldRedirs(TranslatorInterface $translator, Request $request)
    {
        return $this->redirectToRoute('front_partitura');
    }

    /**
     * @Route("/.well-known/webfinger", name="wk_webfinger")
     */
    public function wk_webfinger(TranslatorInterface $translator, Request $request)
    {
        $get = $request->query->all();
        $resource = @$get["resource"];
        $user = preg_replace("/acct:([^@]+).*/","$1", $resource);
        if(!preg_match("/^acct:(partiturak)@".$_SERVER["HTTP_HOST"]."$/", $resource))
        {
            throw $this->createNotFoundException('User not exist.');
        }

        $data = [
            'subject' => $resource,
            'aliases' => [
                "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
            ],
            'links' => [
                [
                    "rel" => "http://webfinger.net/rel/profile-page",
                    "type" => "text/html",
                    "href" => "https://".$_SERVER["HTTP_HOST"]."/",
                ],
                [
                    'rel' => 'self',
                    'type' => 'application/activity+json',
                    'href' => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                ],
                [
                    'rel' => 'http://webfinger.net/rel/avatar',
                    'type' => 'image/png',
                    'href' => "https://".$_SERVER["HTTP_HOST"]."/images/logo.png",
                ]
            ]
        ];
        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/jrd+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/activityPub/users/{user}", name="ac_users_partiturak")
     */
    public function ac_users_partiturak(TranslatorInterface $translator, Request $request, $user)
    {
        if(!preg_match("/^(partiturak)$/", $user))
        {
            throw $this->createNotFoundException('User not exist.');
        }
        $public_key_file =  $this->getParameter('public_key');
        $public_key = file_get_contents($public_key_file);

        $data =
            [
                "@context"=>"https://www.w3.org/ns/activitystreams",
                'type' => 'Person',
                'id' => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                'url' => "https://".$_SERVER["HTTP_HOST"]."/",
                'name' => $user,
                "manuallyApprovesFollowers"=> false,
                "discoverable"=> true,
                "indexable"=> true,
                'preferredUsername' => $user,
                'summary' => 'Partiturak.eus-en mastodon kontua',
                'inbox' => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/inbox",
                'outbox' => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/outbox",
                "following"=> "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/following",
                "followers"=> "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/followers",

                "icon" => [
                    "type" => "Image",
                    "mediaType" => "image/png",
                    "url" => "https://".$_SERVER["HTTP_HOST"]."/images/logo2.png"
                ],
                "image" => [
                    "type" => "Image",
                    "mediaType" => "image/jpeg",
                    "url" => "https://".$_SERVER["HTTP_HOST"]."/images/menubg.jpg"
                ],
                "publicKey"=> [
                    "id"=> "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user#main-key",
                    "owner" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                    "publicKeyPem" => $public_key,
                ]

            ];

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }


    private function getActor($actor)
    {
        $ch=curl_init($actor);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch ,CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Accept: application/json'
            )
        );
        $result = curl_exec($ch);
        curl_close($ch);
        $actor =  json_decode($result,true);
        if(!isset($actor) || !isset($actor['inbox']))
        {
            throw $this->createNotFoundException('Cannot get actor data.');
        }
        return $actor;
    }

    private function makeRequest($actorInbox, $user, $message)
    {
        $publicKeyId = "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user#main-key";
        $date_gmt = new \DateTime();
        $date = $date_gmt->format(DATE_RFC7231);


        $message_str = json_encode($message);
        $sha256 = hash('sha256', $message_str, true);
        $digest = 'SHA-256=' . base64_encode($sha256);

        $inboxUrl = parse_url($actorInbox);


        $sigparts = [
            '(request-target)' => 'post ' . $inboxUrl['path'],
            'host' => $inboxUrl['host'],
            'date' => $date,
            'digest' => $digest,
        ];
        $dataToSign = join("\n", array_map(
            fn($k, $v) => "{$k}: {$v}",
            array_keys($sigparts),
            array_values($sigparts)
        ));

        $private_key_file =  $this->getParameter('private_key');
        $privateKey = openssl_pkey_get_private(file_get_contents($private_key_file));

        $signature = null; // mutated by openssl
        $enodedSignature = null;
        if (!openssl_sign($dataToSign, $signature, $privateKey, OPENSSL_ALGO_SHA256)) {
            throw new Exception('Could not sign activityPub data');
        }
        $signatureHeader = sprintf( 'keyId="%s",algorithm="rsa-sha256",headers="(request-target) host date digest",signature="%s"', $publicKeyId, base64_encode($signature));

        $headers = [
            'accept: application/activity+json, text/json',
            'host: '.$inboxUrl['host'],
            'date: '.$date,
            'digest: '.$digest,
            'signature: '.$signatureHeader,
            'content-type: application/activity+json',
        ];
        $ch=curl_init($actorInbox);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message_str);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        curl_close($ch);
        error_log("params: ".json_encode($message)." / $actorInbox\n", 3, "/tmp/inbox.data");
        error_log("TO: ".json_encode($inboxUrl['host'])." / $actorInbox\n", 3, "/tmp/inbox.data");
        error_log("Make request: ".json_encode($result)."\n", 3, "/tmp/inbox.data");
        return json_decode($body,true);
    }

    private function guidv4($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);

        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }


    /**
     * @Route("/activityPub/users/{user}/inbox", name="ac_users_partiturak_inbox")
     */
    public function ac_users_partiturak_inbox(TranslatorInterface $translator, Request $request, $user)
    {
        $get = $request->query->all();
        if(!preg_match("/^(partiturak)$/", $user))
        {
            throw $this->createNotFoundException('User not exist.');
        }
        $path = $request->getPathInfo();
        $body =  file_get_contents('php://input');
        $log = [
            'path' => $path,
            'body' => $body
        ];
        error_log(json_encode($log)."\n", 3, "/tmp/inbox.data");

        if(isset($get['fake_body']))
        {
            $body = $get['fake_body'];
        }

        if(!empty($body))
        {
            $json = json_decode($body,true);

            // Follow request
            if($json['type'] == 'Follow')
            {
                $actor = $this->getActor($json['actor']);

                // All OK, Save the new follower...
                $repo_fw = $this->getDoctrine()->getRepository(FollowerMastodon::class);
                $check_db = $repo_fw->findBy(['inboxUrl' => $actor['inbox'], 'name' => $user]);
                if(count($check_db)===0)
                {
                    $fw = new FollowerMastodon();
                    $fw->setInboxUrl($actor['inbox']);
                    $fw->setName($user);
                    $entityManager = $this->getDoctrine()->getManager();

                    $entityManager->persist($fw);
                    $entityManager->flush();
                }
                $result = $this->makeRequest($actor['inbox'], $user,  [
                    "@context" => "https://www.w3.org/ns/activitystreams",
                    "summary" => "$user accepted a follow",
                    "type" => "Accept",
                    "actor" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                    "object" => $json['id'],
                ]);
                error_log("FOllow result: ".json_encode($result)."\n", 3, "/tmp/outbox.data");
                if(isset($result['error']))
                {
                    $response = new Response();
                    $response->setContent(json_encode([
                        'error' => 'Error sending Follow accept confirmation',
                        'received' => $result
                    ]));
                    $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
                    return $response;
                }

            }
            // Unfollow request
            if($json['type'] == 'Undo' && $json['object']['type']=='Follow')
            {
                $actor = $this->getActor($json['actor']);
                $repo_fw = $this->getDoctrine()->getRepository(FollowerMastodon::class);
                $check_db = $repo_fw->findBy(['inboxUrl' => $actor['inbox'], 'name' => $user]);
                $entityManager = $this->getDoctrine()->getManager();
                foreach($check_db as $follower)
                {
                    $entityManager->remove($follower);
                    $entityManager->flush();
                }
            }
            // Receiving a message from customer
            if($json['type'] == 'Create')
            {
                $actor = $this->getActor($json['actor']);
                $date = date("Y-m-d\TH:i:sp");
                $message_id = $this->guidv4();
                $result = $this->makeRequest($actor['inbox'], $user,  [
                    "@context" => "https://www.w3.org/ns/activitystreams",
                    "type"=> "Create",
                    "actor" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                    "id" =>  "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/priv-".$message_id."/activity",
                    "to" => [
                        $json['actor']
                    ],
                    "cc" => [
                    ],
                    "object" => [
                        "id" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/priv-".$message_id,
                        "type" => "Note",
                        "summary" => null,
                        "inReplyTo" => null,
                        "published" => $date,
                        "inReplyTo"=> $json['object']['id'],
                        "inReplyAtomUri"=> $json['object']['id'],
                        "url" => "https://".$_SERVER["HTTP_HOST"]."/ikusi/#".$message_id,
                        "attributedTo" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                        "to" => [ $json['actor'] ],
                        "cc" => [ ],
                        "sensitive" => false,
                        "atomUri" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/priv-".$message_id,
                        "inReplyToAtomUri" => null,
                        "conversation" => "tag:mastodon.social,".date("Y-m-d").":objectId=:objectType=Conversation",
                        "content" => "<p><span class=\"h-card\" translate=\"no\">Bot bat naiz. Bidali email bat admin@partiturak.eus-era erantzuna bat jasotzeko.</span></p>",
                        "contentMap" => [ ],
                        "attachment" => [ ],
                        "tag" => [],

                    ],
                    "published" =>  $date
                ]);
            }
        }

        // Default inbox response
        $data =
            [
                '@context' => 'https://www.w3.org/ns/activitystreams',
                'summary' => "Inbox for ".$user,
                'type' => 'OrderedCollection',
                'totalItems' => 0,
                'orderedItems' => [],
            ];

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/activityPub/users/{user}/outbox", name="ac_users_partiturak_outbox")
     */
    public function ac_users_partiturak_outbox(TranslatorInterface $translator, Request $request, $user)
    {
        $get = $request->query->all();
        $path = $request->getPathInfo();
        $body =  file_get_contents('php://input');
        $log = [
            'path' => $path,
            'get' => $_GET,
            'post' => $_POST,
            'body' => $body
        ];
        error_log(json_encode($log)."\n", 3, "/tmp/outbox.data");

        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $filtered = $repo->findAllMastodon(@$get['min_id'], @$get['max_id'] , 20);
        $all = $repo->findAll();

        if(isset($get['page']))
        {
            $last = end($filtered);
            $items = [];
            foreach($filtered as $item)
            {
                $date = $item->getUpdatedAt()->format("Y-m-d\TH:i:sp");


                $items[] =
                    [
                        "id" =>  "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/".$item->getId()."/activity",
                        "type" =>  "Announce",
                        "actor" =>  "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/tfe",
                        "published" =>  $date,
                        "to" =>  [
                            "https://www.w3.org/ns/activitystreams#Public"
                        ],
                        "object" =>  "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/".$item->getId()
                    ];
            }
            $data =
                [
                    '@context' => 'https://www.w3.org/ns/activitystreams',
                    "id"=> 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox?'.$_SERVER["QUERY_STRING"],
                    "type"=> "OrderedCollectionPage",
                    'next' => $last ? 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox?max_id='.$last->getId().'&page=true' : null,
                    'prev' => count($filtered)>0 ? 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox?min_id='.$filtered[0]->getId().'&page=true' : null,
                    "orderedItems" => $items
                ];
        }
        else
        {
            $data =
                [
                    '@context' => 'https://www.w3.org/ns/activitystreams',
                    'id' => 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox',
                    'type' => 'OrderedCollection',
                    'totalItems' => count($all),
                    'first' => 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox?page=true',
                    'last' => 'https://'.$_SERVER["HTTP_HOST"].'/activityPub/users/'.$user.'/outbox?min_id=0&page=true',
                ];
        }

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/activityPub/users/{user}/statuses/{id}/activity", name="ac_users_partiturak_post")
     */
    public function ac_users_partiturak_post(TranslatorInterface $translator, Request $request, $user, Partitura $partitura)
    {
        $get = $request->query->all();
        $path = $request->getPathInfo();
        $body =  file_get_contents('php://input');
        $log = [
            'path' => $path,
            'body' => $body
        ];
        error_log(json_encode($log)."\n", 3, "/tmp/activity.data");

        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $all = $repo->findAllMastodon(@$get['min_id'], @$get['max_id'] , 20);

        $date = $partitura->getUpdatedAt()->format("Y-m-d\TH:i:sp");
        $data =
            [
                '@context' => 'https://www.w3.org/ns/activitystreams',
                "id" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/".$partitura->getId(),
                "type" => "Note",
                "summary" => null,
                "inReplyTo" => null,
                "published" => $date,
                "url" => "https://".$_SERVER["HTTP_HOST"]."/ikusi/".$partitura->getUrl(),
                "attributedTo" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                "to" => [
                    "https://www.w3.org/ns/activitystreams#Public"
                ],
                "sensitive" => false,
                "atomUri" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/".$partitura->getId(),
                "inReplyToAtomUri" => null,
                "conversation" => "tag:mastodon.social,2024-01-29:objectId=631159384:objectType=Conversation",
                "content" => "Partitura PDF ? ....".$partitura->getName(),
                "contentMap" => [
                    "eu" => ""
                ],
                "attachment" => [],
                "tag" => [],

            ];

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/activityPub/users/{user}/followers", name="ac_users_partiturak_followers")
     */
    public function ac_users_partiturak_followers(TranslatorInterface $translator, Request $request, $user)
    {
        $get = $request->query->all();
        $path = $request->getPathInfo();
        $body =  file_get_contents('php://input');
        $log = [
            'path' => $path,
            'body' => $body
        ];
        error_log(json_encode($log)."\n", 3, "/tmp/followers.data");

        $repo_fw = $this->getDoctrine()->getRepository(FollowerMastodon::class);
        $check_db = $repo_fw->findByName($user);
        $data =
            [
                "@context" => "https://www.w3.org/ns/activitystreams",
                "id" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/followers",
                "type" => "OrderedCollection",
                "totalItems" => count($check_db),
                "first" => "https://mastodon.social/users/tfe/followers?page=1"
            ];

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route("/activityPub/users/{user}/following", name="ac_users_partiturak_following")
     */
    public function ac_users_partiturak_following(TranslatorInterface $translator, Request $request, $user)
    {
        $get = $request->query->all();
        $path = $request->getPathInfo();
        $body =  file_get_contents('php://input');
        $log = [
            'path' => $path,
            'body' => $body
        ];
        error_log(json_encode($log)."\n", 3, "/tmp/followers.data");

        $data =
            [
                "@context" => "https://www.w3.org/ns/activitystreams",
                "id" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/following",
                "type" => "OrderedCollection",
                "totalItems" => 0
            ];

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/activity+json; charset=utf-8');
        return $response;
    }

    /**
     * @Route({"es": "/indice.html", "eus":"/", "fr":"index.html"}, name="front_partitura")
     */
    public function index(TagRepository $tagRepo, TranslatorInterface $translator, Request $request)
    {
        $page = 1;
        $get = $request->query->all();

        $repo = $this->getDoctrine()->getRepository(Partitura::class);

        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('search_partitura'),
            'method' => 'get',
        ]);
        $filter_form->handlerequest($request);

        $num_per_pages = 10;
        $partituras = $repo->findLatest($page, $num_per_pages, $filter);

        $keywords = $this->default_keywords;
        $repo = $this->getDoctrine()->getRepository(Instrumento::class);
        $instrumentos = $repo->findAll();
        foreach($instrumentos as $instrumento)
        {
            $keywords[] = $instrumento->getName();
        }


        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        $popular_authors = $tagRepo->popularTags(['isAuthor' => true]);
        $popular_types = $tagRepo->popularTags(['isType' => true]);

        return $this->render('front_partitura/index.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'popular_authors' => $popular_authors,
            'popular_types' => $popular_types,
            'filter_form' => $filter_form->createView(),
            'robots' => count(array_keys($request->query->all()))>0 ? 'noindex' : '',
            'user_favs' => $user_favs,
            'filter' => $filter,
            'keywords' => join("," ,$keywords),
        ]));
    }

    /**
     * @Route({"es": "/buscar", "eus":"/bilatu", "fr":"recherche"}, name="search_partitura")
     */
    public function search(TagRepository $tagRepo, TranslatorInterface $translator, Request $request)
    {
        $get = $request->query->all();
        $page = !empty($get["page"]) && preg_match("/^\d+$/",$get["page"]) ? $get["page"] : 1;;

        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('search_partitura'),
            'method' => 'get',
        ]);
        $filter_form->handlerequest($request);

        $num_per_pages = 50;
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $partituras = $repo->findLatest($page, $num_per_pages, $filter);

        $keywords = $this->default_keywords;
        $repo = $this->getDoctrine()->getRepository(Instrumento::class);
        $instrumentos = $repo->findAll();
        foreach($instrumentos as $instrumento)
        {
            $keywords[] = $instrumento->getName();
        }


        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        $popular_authors = $tagRepo->popularTags(['isAuthor' => true]);
        $popular_types = $tagRepo->popularTags(['isType' => true]);

        return $this->render('front_partitura/search.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'popular_authors' => $popular_authors,
            'popular_types' => $popular_types,
            'num_pages' => ceil($partituras->count() / $num_per_pages),
            'current_page' => $page,
            'robots' => count(array_keys($request->query->all()))>0 ? 'noindex' : '',
            'user_favs' => $user_favs,
            'filter' => $filter,
            'filter_form' => $filter_form->createView(),
            'keywords' => join("," ,$keywords),
        ]));
    }
    private function defaultViewContent()
    {
        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('search_partitura'),
            'method' => 'get',
        ]);
        return
            [
                'filter_form' => $filter_form->createView(),
            ];
    }

    /**
     * @Route({"es": "/rss_es", "eus":"/rss_eus", "fr":"rss_fr"}, name="front_partitura_rss")
     */
    public function rss(TranslatorInterface $translator, Request $request)
    {
        $page = 1;
        $repo = $this->getDoctrine()->getRepository(Partitura::class);

        $filter = new Filter();
        $filter->setCol('createdAt');
        $filter->setSort('desc');

        $num_per_pages = 10;
        $partituras = $repo->findLatest($page, $num_per_pages, $filter);

        $keywords = $this->default_keywords;
        $repo = $this->getDoctrine()->getRepository(Instrumento::class);
        $instrumentos = $repo->findAll();
        foreach($instrumentos as $instrumento)
        {
            $keywords[] = $instrumento->getName();
        }


        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;

        return $this->render('front_partitura/rss.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'user_favs' => $user_favs,
            'filter' => $filter,
            'keywords' => join("," ,$keywords),
            'num_pages' => ceil($partituras->count() / $num_per_pages),
            'current_page' => $page,
        ]));
    }

    /**
     * @Route({"eus": "/musescore/tutorial.eus", "es":"/musescore/tutorial.es", "fr":"/musescore/tutorial.fr"}, name="front_partitura_tuto4")
     */
    public function musescore(Request $request, TranslatorInterface $translator)
    {
        return $this->render('front_partitura/musescore4.html.twig', array_merge($this->defaultViewContent(),[
        ]));
    }
    /**
     * @Route({"eus": "/musescore3/tutorial.eus", "es":"/musescore3/tutorial.es", "fr":"/musescore3/tutorial.fr"}, name="front_partitura_tuto3")
     */
    public function musescore3(Request $request, TranslatorInterface $translator)
    {
        return $this->render('front_partitura/musescore3.html.twig', array_merge($this->defaultViewContent(), [
        ]));
    }


    /**
     * @Route({"eus":"/kategoria/{key}/{page}", "fr":"/type/{key}/{page}", "es":"/por_tipo/{key}/{page}" }, name="by_type", defaults={"key":"","page":"1"})
     */
    public function by_type(TagRepository $tagRepo, Request $request, $page, $key)
    {
        $filter = new Filter();
        $filter->setCol('createdAt');
        $filter->setSort('desc');


        $locale = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repo_tag = $this->getDoctrine()->getRepository(Tag::class);
        $num_per_pages = 18;
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        if(!empty($key))
        {
            $type = $repo_tag->findByName($key);
            if(!$type)
            {
                throw $this->createNotFoundException('Does not exist.');
            }
            $type = $type[0];
            $partituras = $repo->by_tag($type, $page, $num_per_pages, $filter);
        }
        else
        {
            $partituras = null;
            $type = null;
        }

        $keywords = $this->default_keywords;
        $tags = $repo_tag->findBy(['isType' => true], ['name' => 'ASC']);
        $test = $tags[41]->getPartituras();

        $text = null;
        if($type)
        {
            $keywords[] = $type->getName();
            foreach($type->getTagTexts() as $tagText)
            {
                if($tagText->getLocale() == $locale)
                {
                    $text = $tagText;
                }
            }
            $max_page = ceil($partituras->count() / $num_per_pages);
            if($page > $max_page && $page>1)
            {
                return $this->redirectToRoute('front_partitura');
            }

            return $this->render('front_partitura/by.html.twig', array_merge($this->defaultViewContent(), [
                'user_favs' => $user_favs,
                'partituras' => $partituras,
                'robots' => $page>1 ? 'noindex' : '',
                'filter'=> $filter,
                'current_page' => $page,
                'num_pages' => $max_page,
                'keywords' => join("," ,$keywords),
                'type' => 'type',
                'description' => $text ? $text->getText() : '',
                'tag_text' => $text,
                'key' => $key,
            ]));
        }
        $popular_tags = $tagRepo->popularTags(['isType' => true]);
        return $this->render('front_partitura/byempty.html.twig', array_merge($this->defaultViewContent(),[
            'user_favs' => $user_favs,
            'partituras' => $partituras,
            'popular_tags' => $popular_tags,
            'keywords' => join("," ,$keywords),
            'type' => 'type',
            'key' => $key,
            'tags' => $tags,
        ]));
    }

    /**
     * @Route({"eus":"/musika-tresna/{key}/{page}", "fr":"/instruments/{key}/{page}", "es":"/instrumentos/{key}/{page}" }, name="by_instrument", defaults={"key":"", "page":"1"})
     */
    public function by_instruments(Request $request,$page, $key)
    {
        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('by_instrument', ['key' => $key, 'page' => 1]),
            'method' => 'get',
        ]);
        $filter_form->handlerequest($request);

        $locale = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repo_tag = $this->getDoctrine()->getRepository(Tag::class);
        $repo_instrumento = $this->getDoctrine()->getRepository(Instrumento::class);
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $num_per_pages = 10;
        if(!empty($key))
        {
            $instrumento = $repo_tag->findByName($key);
            if(!$instrumento)
            {
                throw $this->createNotFoundException('Does not exist.');
            }
            $type = $instrumento[0];
            $partituras = $repo->by_tag($instrumento, $page, $num_per_pages, $filter);
        }
        else
        {
            $partituras = null;
            $type = null;
        }

        $keywords = $this->default_keywords;
        $tags = $repo_instrumento->findBy([], ['name' => 'ASC']);
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;

        $text = null;
        if($type)
        {
            $keywords[] = $type->getName();
            return $this->render('front_partitura/by.html.twig', array_merge($this->defaultViewContent(),[
                'current_page' => $page,
                'user_favs' => $user_favs,
                'robots' => $page>1 ? 'noindex' : '',
                'filter_form' => $filter_form->createView(),
                'filter' => $filter,
                'partituras' => $partituras,
                'num_pages' => ceil($partituras->count() / $num_per_pages),
                'keywords' => join("," ,$keywords),
                'type' => 'instrument',
                'description' => $text ? $text->getText() : '',
                'tag_text' => $text,
                'key' => $key,
                'tags' => $tags,
            ]));
        }
        return $this->render('front_partitura/instrumentos.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'user_favs' => $user_favs,
            'keywords' => join("," ,$keywords),
            'type' => 'instrument',
            'key' => $key,
            'tags' => $tags,
        ]));
    }

    /**
     * @Route({ "eus": "/autorea/{key}/{page}", "es":"/autor/{key}/{page}", "fr":"/auteur/{key}/{page}"}, name="by_author", defaults={"key":"", "page":"1"})
     */
    public function by_author(TagRepository $tagRepo, Request $request, $page, $key)
    {
        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('by_author', ['key' => $key, 'page' => 1]),
            'method' => 'get',
        ]);
        $filter_form->handlerequest($request);

        $locale = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repo_tag = $this->getDoctrine()->getRepository(Tag::class);
        $num_per_pages = 18;
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;

        $author = null;
        if(!empty($key))
        {
            $author = $repo_tag->findByName($key);
            if(!$author)
            {
                throw $this->createNotFoundException('Does not exist.');
            }
            $author = $author[0];
            $partituras = $repo->by_tag($author, $page, $num_per_pages, $filter);
        }
        else
        {
            $partituras = null;
        }
        $tags = $repo_tag->findBy(['isAuthor' => true], ['name' => 'ASC']);
        $text = null;
        $keywords = $this->default_keywords;
        if($author)
        {
            $keywords[] = $author->getName();
            foreach($author->getTagTexts() as $tagText)
            {
                if($tagText->getLocale() == $locale)
                {
                    $text = $tagText;
                }
            }
            $max_page = ceil($partituras->count() / $num_per_pages);
            if($page > $max_page && $page>1)
            {
                return $this->redirectToRoute('front_partitura');
            }
            return $this->render('front_partitura/by.html.twig', array_merge($this->defaultViewContent(),[
                'partituras' => $partituras,
                'robots' => $page>1 ? 'noindex' : '',
                'user_favs' => $user_favs,
                'filter_form' => $filter_form->createView(),
                'filter' => $filter,
                'keywords' => join("," ,$keywords),
                'type' => 'author',
                'current_page' => $page,
                'num_pages' => $max_page,
                'tag' => $author,
                'description' => $text ? $text->getText() : '',
                'tag_text' => $text,
                'key' => $key,
                'tags' => $tags,
            ]));
        }

        $popular_tags = $tagRepo->popularTags(['isAuthor' => true]);

        return $this->render('front_partitura/byempty.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'user_favs' => $user_favs,
            'popular_tags' => $popular_tags,
            'keywords' => join("," ,$keywords),
            'type' => 'author',
            'key' => $key,
            'tags' => $tags,
        ]));

    }

    /**
     * @Route({ "eus": "/jatorria/{key}/{page}", "es":"/origen/{key}/{page}", "fr":"/provenance/{key}/{page}"}, name="by_origin", defaults={"key":"", "page":"1"})
     */
    public function by_origin(Request $request, $page, $key)
    {
        $filter = new filter();
        $filter->setcol('createdat');
        $filter->setsort('desc');
        $filter_form = $this->createform(filtertype::class, $filter, [
            'action' => $this->generateurl('by_origin', ['key' => $key, 'page' => 1]),
            'method' => 'get',
        ]);
        $filter_form->handlerequest($request);

        $locale = $request->getLocale();
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repo_tag = $this->getDoctrine()->getRepository(Tag::class);
        $num_per_pages = 18;
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;

        $origin = null;
        if(!empty($key))
        {
            $origin = $repo_tag->findByName($key);
            if(!$origin)
            {
                throw $this->createNotFoundException('Does not exist.');
            }
            $origin = $origin[0];
            $partituras = $repo->by_tag($origin, $page, $num_per_pages, $filter);
        }
        else
        {
            $partituras = null;
        }
        $tags = $repo_tag->findBy(['isOrigin' => true], ['name' => 'ASC']);
        $text = null;
        $keywords = $this->default_keywords;
        if($origin)
        {
            $keywords[] = $origin->getName();
            foreach($origin->getTagTexts() as $tagText)
            {
                if($tagText->getLocale() == $locale)
                {
                    $text = $tagText;
                }
            }
            $max_page = ceil($partituras->count() / $num_per_pages);
            if($page > $max_page && $page>1)
            {
                return $this->redirectToRoute('front_partitura');
            }
            return $this->render('front_partitura/by.html.twig', array_merge($this->defaultViewContent(),[
                'partituras' => $partituras,
                'user_favs' => $user_favs,
                'robots' => $page>1 ? 'noindex' : '',
                'filter_form' => $filter_form->createView(),
                'filter' => $filter,
                'keywords' => join("," ,$keywords),
                'type' => 'origin',
                'current_page' => $page,
                'num_pages' => $max_page,
                'tag' => $origin,
                'description' => $text ? $text->getText() : '',
                'tag_text' => $text,
                'key' => $key,
                'tags' => $tags,
            ]));
        }
        return $this->render('front_partitura/byempty.html.twig', array_merge($this->defaultViewContent(),[
            'partituras' => $partituras,
            'user_favs' => $user_favs,
            'keywords' => join("," ,$keywords),
            'type' => 'origin',
            'key' => $key,
            'tags' => $tags,
        ]));

    }
    /**
     * @Route({"eus": "/prop", "fr": "/proposer", "es":"/proponer"}, * name="front_partitura_propose", methods={"GET","POST"})
     */
    public function propose(Request $request, TranslatorInterface $translator)
    {
    }

    /**
     * @Route({"eus": "/sortupartitura", "fr": "/creerpartition", "es":"/subirpartitura"}, * name="front_partitura_create", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request, TranslatorInterface $translator,  UserPasswordEncoderInterface $passwordEncoder)
    {
        if(!$this->security->isGranted('ROLE_USER'))
        {
            throw $this->createNotFoundException('Denied.');
        }
        $partitura = new Partitura();
        $user = $this->security->getUser();

        if(!$this->security->isGranted('ROLE_ADMIN'))
        {
            $partitura->setVisible(false);
        }
        $partitura->setUser($user);

        $form = $this->createForm(PartituraFirstType::class, $partitura,['creation' => true]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $fileName = $partitura->getMusescoreFileFile()->getClientOriginalName();
            $partitura->setName(preg_replace("/\..*?$/","", $fileName));

            $baseUrl = preg_replace("/[^a-zA-Z0-9_\-]+/","_",strtolower($partitura->getName()));
            $partitura->setUrl($baseUrl);

            $id="";
            $checkUrl = $baseUrl;
            $repo = $this->getDoctrine()->getRepository(Partitura::class);
            do
            {
                $checkUrl = $baseUrl.$id;
                $check_partitura = $repo->findByUrl($checkUrl);
                $id = empty($id) ? 2 : $id+1;
            }
            while(count($check_partitura)>0 && $id<50);

            $partitura->setUrl($checkUrl);

            if(!$this->security->isGranted('ROLE_ADMIN'))
            {
                $partitura->setVisible(false);
            }
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($partitura);
            $entityManager->flush();
            return $this->redirectToRoute('partitura_edit', array("id" => $partitura->getId()));
        }
        return $this->render('partitura/first.html.twig', array_merge($this->defaultViewContent(),[
            'partitura' => $partitura,
            'user' => $user,
            'form' => $form->createView(),
        ]));
    }

    /**
     * @Route("/generic_data", * name="front_partitura_data", methods={"GET"})
     */
    public function generic_data()
    {
        $data = [];
        $cacheFile = "/tmp/generic_data.cache4";

        /* 100 minutes cache */
        if(file_exists($cacheFile) && filemtime($cacheFile) > time()- 6000)
        {
            $data = file_get_contents($cacheFile);
        }
        else
        {
            $repoTags = $this->getDoctrine()->getRepository(Tag::class);
            $tags = $repoTags->findBy([], ['name' => 'ASC']);

            $repoInstru = $this->getDoctrine()->getRepository(Instrumento::class);
            $instrumentos = $repoInstru->findBy([], ['name' => 'ASC']);

            $repoParti = $this->getDoctrine()->getRepository(Partitura::class);
            $partituras = $repoParti->findBy([], ['name' => 'ASC']);


            $tagsSend = [];
            $tagsByName=[];
            foreach($tags as $tag)
            {
                $tagsByName[] = $tag->getId();
                $tagsSend[$tag->getId()] = [
                    'id' => $tag->getId(),
                    'name' => $tag->getName(),
                    'isAuthor' => $tag->getIsAuthor(),
                    'isType' => $tag->getIsType(),
                    'isInstrument' => $tag->getIsInstrument(),
                    'isOrigin' => $tag->getIsOrigin()
                ];
            }

            $instruSend = [];
            foreach($instrumentos as $instrumento)
            {
                $instruSend[$instrumento->getId()] = [
                    'name' => $instrumento->getName(),
                    'image' => 'upload/image/'.$instrumento->getImage(),
                    'volume' => $instrumento->getVolume(),
                    'tag' => $instrumento->getTag(),
                ];
            }

            $partiturasSend = [];
            $indexTags = [];
            foreach($partituras as $partitura)
            {
                if($partitura->getVisible())
                {
                    $tags = [];
                    foreach($partitura->getTags() as $tag)
                    {
                        $tagId = $tag->getId();
                        $tags[] = $tagId;
                        if(!isset($indexTags[$tagId]))
                        {
                            $indexTags[$tagId]=[];
                        }
                        $indexTags[$tagId][] = $partitura->getId();
                    }
                    $mp3s = [];
                    foreach($partitura->getMp3s() as $mp3)
                    {
                        $mp3s[] = [
                            'instrument' => $mp3->getInstrumento()->getId(),
                            'tag' => $mp3->getInstrumento()->getTag()->getId(),
                            'name' => $mp3->getName(),
                            'mp3' => 'upload/mp3/'.$mp3->getMp3(),
                        ];
                    }
                    $pdfs = [];

                    $generic_path = getcwd();

                    foreach($partitura->getPartituraPdfs() as $pdf)
                    {
                        $pdfs[] = [
                            'name' => $pdf->getName(),
                            'pdf' => 'upload/pdf/'.$pdf->getPdf(),
                        ];
                    }
                    $env = array('HOME' => '/tmp/', 'PATH' => '/usr/local/bin:/usr/bin:/bin');

                    $partiturasSend[$partitura->getId()] = [
                        'id' => $partitura->getId(),
                        'name' => $partitura->getName(),
                        'url' => $partitura->getUrl(),
                        'views' => $partitura->getViews(),
                        'text' => $partitura->getText(),
                        'credit' => $partitura->getCredito(),
                        'creditUrl' => $partitura->getCreditoUrl(),
                        'description' => $partitura->getDescription(),
                        'mainPdf' => 'upload/musescore/'.$partitura->getMusescoreFile(),
                        'date' => $partitura->getCreatedAt(),
                        'tags' => $tags,
                        'mp3s' => $mp3s,
                        'pdfs' => $pdfs
                    ];
                }
            }

            $data = json_encode([
                'instruments' => $instruSend,
                'tags' => $tagsSend,
                'tagsByName' => $tagsByName,
                'musicsheets' => $partiturasSend,
                'musicsheetsByTags' => $indexTags,
                'result' => true
            ]);
            file_put_contents($cacheFile, $data);
        }

        $response = new Response();
        $response->setContent($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/generic_tags", * name="front_partitura_data2", methods={"GET"})
     */
    public function generic_tags()
    {
        $data = [];
        $cacheFile = "/tmp/generic_tags.cache4";

        /* 100 minutes cache */
        if(file_exists($cacheFile) && filemtime($cacheFile) > time()- 6000)
        {
            $data = file_get_contents($cacheFile);
        }
        else
        {
            $repoTags = $this->getDoctrine()->getRepository(Tag::class);
            $tags = $repoTags->findBy([], ['name' => 'ASC']);
            $popular_authors = $repoTags->popularTags(['isAuthor' => true]);
            $popular_types = $repoTags->popularTags(['isType' => true]);

            $tagsSend = [];
            foreach($tags as $tag)
            {
                $tagsSend[] = [
                    'id' => $tag->getId(),
                    'name' => $tag->getName(),
                    'isAuthor' => $tag->getIsAuthor(),
                    'isType' => $tag->getIsType(),
                    'isInstrument' => $tag->getIsInstrument(),
                    'isOrigin' => $tag->getIsOrigin()
                ];
            }
            $popTypes = [];
            foreach($popular_types as $data)
            {
                $popTypes[] = [
                    'id' => $data['tag']->getId(),
                    'name' => $data['tag']->getName(),
                    'num' => $data['num']
                ];
            }
            $popAuthors = [];
            foreach($popular_authors as $data)
            {
                $popAuthors[] = [
                    'id' => $data['tag']->getId(),
                    'num' => $data['num'],
                    'name' => $data['tag']->getName(),
                ];
            }

            $data = json_encode([
                'tags' => $tagsSend,
                'popular_authors' => $popAuthors,
                'popular_types' => $popTypes,
                'result' => true
            ]);
            file_put_contents($cacheFile, $data);
        }

        $response = new Response();
        $response->setContent($data);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route({"eus": "/stareus/{id}", "fr": "/starfr/{id}", "es":"/stares/{id}"}, * name="front_partitura_fav", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function fav(Request $request, $id, TranslatorInterface $translator)
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repoFav = $this->getDoctrine()->getRepository(UserFav::class);
        $partitura = $repo->findById($id);
        $entityManager = $this->getDoctrine()->getManager();

        if(!$partitura)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $partitura = $partitura[0];
        $fav = $repoFav->findOneBy(['User' => $this->security->getUser(), 'Partitura' => $partitura]);
        $result = false;
        if(!$fav)
        {
            $fav = new UserFav();
            $fav->setUser($this->security->getUser());
            $fav->setPartitura($partitura);
            $entityManager->persist($fav);
            $entityManager->flush();
            $result = true;
        }
        else
        {
            $entityManager->remove($fav);
            $entityManager->flush();
        }
        $response = new Response();
        $response->setContent(json_encode([
            'result' => $result
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route({"eus": "/ikusi/{old}/{url}", "fr": "/afficher/{old}/{url}", "es":"/ver/{old}/{url}"}, * name="front_partitura_old", methods={"GET","POST"})
     */
    public function old_show(Request $request, $url, TranslatorInterface $translator)
    {
        return $this->redirectToRoute('front_partitura_show', [ 'url' => $url ]);
    }

    /**
     * @Route({"eus": "/ikusi/{url}", "fr": "/afficher/{url}", "es":"/ver/{url}"}, * name="front_partitura_show", methods={"GET","POST"})
     */
    public function show(Request $request, $url, TranslatorInterface $translator)
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $partitura = $repo->findByUrl($url);


        if(!$partitura)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $partitura = $partitura[0];

        $comentario = new Comentario();
        $comentario->setPartitura($partitura);
        $comentario->setValidated(false);
        $comentario->setUser($this->getUser());
        $form = $this->createForm(ComentarioUserType::class, $comentario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comentario);
            $entityManager->flush();
            return $this->redirectToRoute('front_partitura_show', [
                'url' => $partitura->getUrl()
            ]);
        }

        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;

        if(!$partitura->getVisible() && !$this->security->isGranted('ROLE_ADMIN') &&
            $partitura->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        // Get user lists
        $repoUserList = $this->getDoctrine()->getRepository(UserList::class);
        if(!$logged_user)
        {
            $user_lists = [];
        }
        else if(!$this->security->isGranted('ROLE_ADMIN'))
        {
            $user_lists = $repoUserList->findBy(['user' => $logged_user], ['name' => 'ASC']);
        }
        else
        {
            $user_lists = $repoUserList->findBy([], ['name' => 'ASC']);
        }
        $keywords = $this->default_keywords;
        $instruments = [];
        foreach($partitura->getTags() as $tag)
        {
            $keywords[] = $tag->getName();
        }
        foreach($partitura->getMp3s() as $mp3)
        {
            $instrument_name = $mp3->getInstrumento()->getName();
            $keywords[] = $instrument_name;
            if(array_search($instrument_name, $instruments)===false)
            {
                $instruments[] = $instrument_name;
            }
        }

        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;

        if(!$this->security->isGranted('ROLE_ADMIN'))
        {
            $partitura->setViews($partitura->getViews()+1);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($partitura);
            $entityManager->flush();
        }

        // Recommendations:
        // We get the "Type" or Author of the musicsheet and get next/previous
        $type = null;
        $author = null;
        foreach($partitura->getTags() as $tag)
        {
            if($tag->getIsType())
            {
                $type = $tag;
            }
            if($tag->getIsAuthor())
            {
                $author = $tag;
            }
        }
        $recommendations_type = null;
        if($type)
        {
            $recommendations_type = $repo->recommendations($type, $partitura);
        }
        $recommendations_author = null;
        if($author)
        {
            $recommendations_author = $repo->recommendations($author, $partitura);
        }


        return $this->render('front_partitura/view.html.twig', array_merge($this->defaultViewContent(),[
            'partitura' => $partitura,
            'user_favs' => $user_favs,
            'user_lists' => $user_lists,
            'instruments' => $instruments,
            'keywords' => join("," ,$keywords),
            'comment_form' => $form->createView(),
            'description' => 
            $partitura->getName()." - ".
            $translator->trans('Partituras', 
            [
            ])." - ".
            $partitura->getDescription(),

            // Recommendations
            'recommendations_type' => $recommendations_type,
            'type' => $type,
            'author' => $author,
            'recommendations_author' => $recommendations_author,
        ]));
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
