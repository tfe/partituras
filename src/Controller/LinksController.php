<?php

namespace App\Controller;

use App\Entity\Partitura;
use App\Entity\Links;
use App\Form\LinksType;
use App\Repository\LinksRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route({"es":"/editlinkes", "eus":"/editlinkeus","fr":"/editlinkfr"})
 */
class LinksController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new/{id}", name="links_new", methods={"GET","POST"})
     */
    public function new(Request $request, $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $partituras = $repo->findById($id);
        if(!$partituras)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partituras[0]->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        $link = new Links();
        $link->setPartitura($partituras[0]);
        $form = $this->createForm(LinksType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();

            return $this->redirectToRoute('partitura_edit', [
                'id' => $link->getPartitura()->getId(),
            ]);
        }

        return $this->render('links/new.html.twig', [
            'link' => $link,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="links_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Links $link): Response
    {
        $form = $this->createForm(LinksType::class, $link);
        $form->handleRequest($request);

        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $link->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('partitura_edit', [
                'id' => $link->getPartitura()->getId(),
            ]);
        }

        return $this->render('links/edit.html.twig', [
            'link' => $link,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="links_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Links $link): Response
    {
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $link->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $partitura_id = $link->getPartitura()->getId();

        if ($this->isCsrfTokenValid('delete'.$link->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($link);
            $entityManager->flush();
        }

        return $this->redirectToRoute('partitura_edit', [ "id" => $partitura_id]);
    }
}
