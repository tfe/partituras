<?php

namespace App\Controller;

use App\Entity\Partitura;
use App\Entity\User;
use App\Form\PartituraType;
use App\Form\PartituraPdfType;
use App\Repository\PartituraRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\FollowerMastodon;

/**
 * @Route({"es":"/editmusices", "eus":"/editmusiceus","fr":"/editmusicfr"})
 */
class PartituraController extends AbstractController
{
    private $security;
    private $logged_user;

    public function __construct(Security $security)
    {
        $this->security = $security;

    }

    public function getUserData($request)
    {
        $this->logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if($this->logged_user)
        {
            $repo = $this->getDoctrine()->getRepository(User::class);
            $logged_users = $repo->findById($this->logged_user);
            $this->logged_user  =$logged_users[0];
        }
    }

    /**
     * @Route("/", name="partitura_index", methods={"GET"})
     */
    public function index(PartituraRepository $partituraRepository, Request $request): Response
    {
        $this->getUserData($request);
        $partituras = $partituraRepository->findByUser($this->logged_user->getId());

        if($this->security->isGranted('ROLE_ADMIN'))
        {
            $partituras = $partituraRepository->findAll();
        }
        return $this->render('partitura/index.html.twig', [
            'partituras' => $partituras
        ]);
    }

    /**
     * @Route("/new", name="partitura_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->getUserData($request);

        $partitura = new Partitura();
        $partitura->setUser($this->logged_user);
        $form = $this->createForm(PartituraType::class, $partitura);
        $form->handleRequest($request);

        if(!$this->security->isGranted('ROLE_ADMIN'))
        {
            $partitura->setVisible(false);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($partitura);
            $entityManager->flush();

            return $this->redirectToRoute('partitura_edit', array("id" => $partitura->getId()));
        }

        return $this->render('partitura/new.html.twig', [
            'partitura' => $partitura,
            'form' => $form->createView(),
        ]);
    }

    private function makeRequest($actorInbox, $user, $message)
    {
        $publicKeyId = "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user#main-key";
        $date_gmt = new \DateTime();
        $date = $date_gmt->format(DATE_RFC7231);


        $message_str = json_encode($message);
        $sha256 = hash('sha256', $message_str, true);
        $digest = 'SHA-256=' . base64_encode($sha256);

        $inboxUrl = parse_url($actorInbox);


        $sigparts = [
            '(request-target)' => 'post ' . $inboxUrl['path'],
            'host' => $inboxUrl['host'],
            'date' => $date,
            'digest' => $digest,
        ];
        $dataToSign = join("\n", array_map(
            fn($k, $v) => "{$k}: {$v}",
            array_keys($sigparts),
            array_values($sigparts)
        ));

        $private_key_file =  $this->getParameter('private_key');
        $privateKey = openssl_pkey_get_private(file_get_contents($private_key_file));

        $signature = null; // mutated by openssl
        $enodedSignature = null;
        if (!openssl_sign($dataToSign, $signature, $privateKey, OPENSSL_ALGO_SHA256)) {
            throw new Exception('Could not sign activityPub data');
        }
        $signatureHeader = sprintf( 'keyId="%s",algorithm="rsa-sha256",headers="(request-target) host date digest",signature="%s"', $publicKeyId, base64_encode($signature));

        $headers = [
            'accept: application/activity+json, text/json',
            'host: '.$inboxUrl['host'],
            'date: '.$date,
            'digest: '.$digest,
            'signature: '.$signatureHeader,
            'content-type: application/activity+json',
        ];
        $ch=curl_init($actorInbox);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message_str);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        curl_close($ch);
        error_log("params: ".json_encode($message)." / $actorInbox\n", 3, "/tmp/inbox.data");
        error_log("TO: ".json_encode($inboxUrl['host'])." / $actorInbox\n", 3, "/tmp/inbox.data");
        error_log("Make request: ".json_encode($result)."\n", 3, "/tmp/inbox.data");
        return json_decode($body,true);
    }

    /**
     * @Route("/{id}/edit", name="partitura_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Partitura $partitura): Response
    {
        $this->getUserData($request);
        $was_visible = $partitura->getVisible();
        $form = $this->createForm(PartituraType::class, $partitura);
        $pdfForm = $this->createForm(PartituraPdfType::class, null);

        $form->handleRequest($request);

        $logged_user = $this->logged_user ? $this->logged_user->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partitura->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        // Not admin force visibility to false
        if(!$was_visible && !$this->security->isGranted('ROLE_ADMIN'))
        {
            $partitura->setVisible(false);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach($partitura->getTags() as $tag)
            {
                $em->persist($tag);
            }
            $em->persist($partitura);
            $em->flush();

            if($partitura->getVisible())
            {
                // ICI
                $repo_fw = $this->getDoctrine()->getRepository(FollowerMastodon::class);
                $followers = $repo_fw->findAll();
                foreach($followers as $follower)
                {
                    $actorInbox = $follower->getInboxUrl();
                    $user = "partiturak";
                    $date = date("Y-m-d\TH:i:sp");
                    $result = $this->makeRequest($actorInbox, $user,  [
                        "@context" => "https://www.w3.org/ns/activitystreams",
                        "type"=> "Create",
                        "actor" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                        "id" =>  "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/id-".$partitura->getId()."/activity",
                        "to" => [ "https://www.w3.org/ns/activitystreams#Public" ],
                        "cc" => [ "https://www.w3.org/ns/activitystreams#Public" ],
                        "object" => [
                            "id" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/id-".$partitura->getId(),
                            "type" => "Note",
                            "summary" => null,
                            "inReplyTo" => null,
                            "published" => $date,
                            "url" => "https://".$_SERVER["HTTP_HOST"]."/ikusi/".$partitura->getUrl(),
                            "attributedTo" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user",
                            "to" => [ "https://www.w3.org/ns/activitystreams#Public" ],
                            "cc" => [ "https://www.w3.org/ns/activitystreams#Public" ],
                            "sensitive" => false,
                            "atomUri" => "https://".$_SERVER["HTTP_HOST"]."/activityPub/users/$user/statuses/id-".$partitura->getId(),
                            "inReplyToAtomUri" => null,
                            "conversation" => "tag:mastodon.social,".date("Y-m-d").":objectId=:objectType=Conversation",
                            "content" => "<p><span class=\"h-card\" translate=\"no\">Partitura berri bat ikusgai dago partiturak.eus-en.<br><a href=\"https://".$_SERVER["HTTP_HOST"]."/ikusi/".$partitura->getUrl()."\">".$partitura->getName()."</a></span></p>",
                            "contentMap" => [],
                            "attachment" => [
                                [
                                    "height" => 2000,
                                    "mediaType" => "image/jpeg",
                                    "name" => $partitura->getName().".pdf",
                                    "type" => "Document",
                                    "url" => "https://partiturak.eus/upload/musescore/".str_replace('.pdf','.jpg',$partitura->getMusescoreFile())
                                ]
                            ],
                            "tag" => [],
                        ],
                        "published" =>  $date
                    ]);
                }
            }

            return $this->redirectToRoute('front_partitura_show', [
                'url' => $partitura->getUrl()
            ]);
        }

        return $this->render('partitura/edit.html.twig', [
            'partitura' => $partitura,
            'form' => $form->createView(),
            'pdfForm' => $pdfForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="partitura_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Partitura $partitura): Response
    {
        $this->getUserData($request);
        if(!$this->security->isGranted('ROLE_ADMIN') && $partitura->getUser()->getId() != $this->logged_user->getId())
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        if ($this->isCsrfTokenValid('delete'.$partitura->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($partitura);
            $entityManager->flush();
        }

        return $this->redirectToRoute('partitura_index');
    }
}
