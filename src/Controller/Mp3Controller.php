<?php

namespace App\Controller;

use App\Entity\Mp3;
use App\Entity\Partitura;
use App\Form\Mp3Type;
use App\Repository\Mp3Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route({"es":"/editmp3es", "eus":"/editmp3eus","fr":"/editmp3fr"})
 */
class Mp3Controller extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/new/{id}", name="mp3_new", methods={"GET","POST"})
     */
    public function new(Request $request, $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $partituras = $repo->findById($id);
        if(!$partituras)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $partituras[0]->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        $mp3 = new Mp3();
        $mp3->setPartitura($partituras[0]);
        $form = $this->createForm(Mp3Type::class, $mp3);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mp3);
            $entityManager->flush();

            return $this->redirectToRoute('partitura_edit', [
                'id' => $mp3->getPartitura()->getId(),
            ]);
        }

        return $this->render('mp3/new.html.twig', [
            'mp3' => $mp3,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mp3_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mp3 $mp3): Response
    {
        $form = $this->createForm(Mp3Type::class, $mp3);
        $form->handleRequest($request);
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $mp3->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('partitura_edit', [
                'id' => $mp3->getPartitura()->getId(),
            ]);
        }

        return $this->render('mp3/edit.html.twig', [
            'mp3' => $mp3,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mp3_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Mp3 $mp3): Response
    {
        $logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if(!$this->security->isGranted('ROLE_ADMIN') && $mp3->getPartitura()->getUser()->getId() != $logged_user)
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $partitura_id = $mp3->getPartitura()->getId();
        if ($this->isCsrfTokenValid('delete'.$mp3->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mp3);
            $entityManager->flush();
        }

        return $this->redirectToRoute('partitura_edit', [ "id" => $partitura_id]);
    }
}
