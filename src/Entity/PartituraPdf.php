<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartituraPdfRepository")
 * @Vich\Uploadable
 */
class PartituraPdf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="partituraPdfs")
     */
    private $Partitura;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @Vich\UploadableField(mapping="pdf", fileNameProperty="pdf")
     * @Assert\File(mimeTypes={"application/pdf, image/png, image/jpg"})
     * @Assert\File(maxSize="10M")
     * @var File
     */
    private $pdfFile;
    /**
     * @ORM\Column(type="string", length=500)
     */

    private $pdf;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instrumento")
     */
    private $instrumento;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $Name;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->Partitura;
    }

    public function setPartitura(?Partitura $Partitura): self
    {
        $this->Partitura = $Partitura;

        return $this;
    }

    public function getPdf(): ?string
    {
        return $this->pdf;
    }
    public function getPdfFile(): ?File
    {
        return $this->pdfFile;
    }

    public function setPdf($pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }
    /**
     * @param File|UploadedFile $pdfFile
     */
    public function setPdfFile(?File $pdfFile = null)
    {
        $this->pdfFile = $pdfFile;

        if (null !== $pdfFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getInstrumento(): ?Instrumento
    {
        return $this->instrumento;
    }

    public function setInstrumento(?Instrumento $instrumento): self
    {
        $this->instrumento = $instrumento;
        if(!$this->Name)
        {
            $this->setName($instrumento->getName());
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(?string $Name): self
    {
        if(empty($Name))
        {
            $Name = $this->getInstrumento()->getName();
        }
        $this->Name = $Name;

        return $this;
    }

}
