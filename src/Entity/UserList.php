<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\UserListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserListRepository::class)
 */
class UserList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
        @Assert\Regex(pattern="/^[a-zA-Z0-9 _\-áéíóúÁÉÍÓÚ\!]+$/i")
    */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $public;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userLists")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=UserFav::class, mappedBy="userList")
     */
    private $favs;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity=UserListPartitura::class, mappedBy="UserList", orphanRemoval=true)
     * @ORM\OrderBy({"ordernumber" = "ASC"})
     */
    private $userListPartituras;
    private $partituras;

    public function __construct()
    {
        $this->favs = new ArrayCollection();
        $this->userListPartituras = new ArrayCollection();
        $this->partituras = new ArrayCollection();
    }

    public function getPartituras()
    {
        $partituras = [];
        foreach($this->userListPartituras as $userlistPartitura)
        {
            $partituras[] = $userlistPartitura->getPartitura();
        }
        return $partituras;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, UserFav>
     */
    public function getFavs(): Collection
    {
        return $this->favs;
    }

    public function addFav(UserFav $fav): self
    {
        if (!$this->favs->contains($fav)) {
            $this->favs[] = $fav;
            $fav->setUserList($this);
        }

        return $this;
    }

    public function removeFav(UserFav $fav): self
    {
        if ($this->favs->removeElement($fav)) {
            // set the owning side to null (unless already changed)
            if ($fav->getUserList() === $this) {
                $fav->setUserList(null);
            }
        }

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection<int, UserListPartitura>
     */
    public function getUserListPartituras(): Collection
    {
        return $this->userListPartituras;
    }

    public function addUserListPartitura(UserListPartitura $userListPartitura): self
    {
        if (!$this->userListPartituras->contains($userListPartitura)) {
            $this->userListPartituras[] = $userListPartitura;
            $userListPartitura->setUserList($this);
        }

        return $this;
    }

    public function removeUserListPartitura(UserListPartitura $userListPartitura): self
    {
        if ($this->userListPartituras->removeElement($userListPartitura)) {
            // set the owning side to null (unless already changed)
            if ($userListPartitura->getUserList() === $this) {
                $userListPartitura->setUserList(null);
            }
        }

        return $this;
    }
}
