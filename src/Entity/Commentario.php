<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentarioRepository")
 */
class Commentario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="commentarios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $partitura;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="string", length=10000)
     */
    private $Text;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->partitura;
    }

    public function setPartitura(?Partitura $partitura): self
    {
        $this->partitura = $partitura;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->Text;
    }

    public function setText(string $Text): self
    {
        $this->Text = $Text;

        return $this;
    }
}
