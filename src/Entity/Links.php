<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinksRepository")
 */
class Links
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="links")
     * @ORM\JoinColumn(nullable=false)
     */
    private $partitura;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $iframe_content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->partitura;
    }

    public function setPartitura(?Partitura $partitura): self
    {
        $this->partitura = $partitura;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getIframeContent(): ?bool
    {
        return $this->iframe_content;
    }

    public function setIframeContent(bool $iframe_content): self
    {
        $this->iframe_content = $iframe_content;

        return $this;
    }
}
