<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartituraRepository")
 * @UniqueEntity("url")
 * @Vich\Uploadable
 */
class Partitura
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $views;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="text", length=5000, nullable=true)
     */
    private $validationComment;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $credito;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $creditoUrl;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="tags", cascade={"persist"})
     * @OrderBy({"isType" = "DESC", "isAuthor" = "DESC", "name" = "DESC"})
     */
    private $tags;

    /**
     * @Assert\File(maxSize="10M")
     * @Vich\UploadableField(mapping="musescore", fileNameProperty="musescoreFile")
     * @var File
     */
    private $musescoreFileFile;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $musescoreFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $createdAt;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Mp3", mappedBy="partitura", orphanRemoval=true, cascade={"persist"})
     */
    private $mp3s;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Links", mappedBy="partitura", orphanRemoval=true)
     */
    private $links;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="partituras", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comentario", mappedBy="Partitura", orphanRemoval=true)
     */
    private $comentarios;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PartituraPdf", mappedBy="Partitura", cascade={"persist"})
     */
    private $partituraPdfs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserFav", mappedBy="Partitura")
     */
    private $userFavs;

    /**
     * @ORM\OneToMany(targetEntity=UserListPartitura::class, mappedBy="Partitura")
     */
    private $userListPartituras;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->mp3s = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->comentarios = new ArrayCollection();
        $this->partituraPdfs = new ArrayCollection();
        $this->userFavs = new ArrayCollection();

        $this->createdAt = new \DateTime();
        $this->userListPartituras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function getText(): ?string
    {
        return $this->text;
    }
    public function getViews(): ?string
    {
        return $this->views ?? 0;
    }
    public function getValidationComment(): ?string
    {
        return $this->validationComment;
    }
    public function getCredito(): ?string
    {
        return $this->credito;
    }
    public function getCreditoUrl(): ?string
    {
        return $this->creditoUrl;
    }
    public function getCreditoUrlThumbnail(): ?string
    {
        $id = preg_replace("/.*embed\/([^\?]+).*?$/","$1", $this->creditoUrl);

        return "$id/hqdefault.jpg";
    }
    public function __toString()
    {
        return $this->name;
    }


    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }
    public function setViews($views): self
    {
        $this->views = $views;

        return $this;
    }
    public function setValidationComment($validationComment): self
    {
        $this->validationComment = $validationComment;

        return $this;
    }
    public function setCredito($credito): self
    {
        $this->credito = $credito;

        return $this;
    }
    public function setCreditoUrl($creditoUrl): self
    {
        if(preg_match("/www.youtube.com\/watch/", $creditoUrl))
        {
            $creditoUrl = "https://www.youtube.com/embed/".preg_replace("/.*v=([^&]+).*?$/", "$1", $creditoUrl);
        }
        else if(preg_match("/youtu.be\//", $creditoUrl))
        {
            $creditoUrl = "https://www.youtube.com/embed/".preg_replace("/.*youtu.be\/([^\?]+).*$/", "$1", $creditoUrl);
        }
        $this->creditoUrl = $creditoUrl;

        return $this;
    }

    public function getUrl(): ?String
    {
        return $this->url;
    }

    public function setUrl(?String $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $Description): self
    {
        $this->description = $Description;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }


    /**
     * @return Collection|Tag[]
     */
    public function getAuthors(): Collection
    {
        $result = new ArrayCollection();
        foreach( $this->tags as $tag)
        {
            if($tag->getIsAuthor())
            {
                $result[] = $tag;
            }
        }
        return $result;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTypes(): Collection
    {
        $result = new ArrayCollection();
        foreach( $this->tags as $tag)
        {
            if($tag->getIsType())
            {
                $result[] = $tag;
            }
        }
        return $result;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getInstruments(): Collection
    {
        $result = new ArrayCollection();
        foreach( $this->tags as $tag)
        {
            if($tag->getIsInstrument())
            {
                $result[] = $tag;
            }
        }
        return $result;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getOrigins(): Collection
    {
        $result = new ArrayCollection();
        foreach( $this->tags as $tag)
        {
            if($tag->getIsOrigin())
            {
                $result[] = $tag;
            }
        }
        return $result;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function addAuthor(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
    public function removeAuthor(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    public function getMusescoreFileFile(): ?File
    {
        return $this->musescoreFileFile;
    }
    public function getMusescoreFileContent(): ?File
    {
        return file_get_contents($this->musescoreFile);
    }
    /**
     * @param File|UploadedFile $MusescoreFileFile
     */
    public function setMusescoreFileFile(?File $MusescoreFileFile = null)
    {
        $this->musescoreFileFile = $MusescoreFileFile;

        if (null !== $MusescoreFileFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function addType(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeType(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
    public function addInstrument(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeInstrument(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
    public function addOrigin(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeOrigin(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
    public function getMusescoreFile(): ?string
    {
        return $this->musescoreFile;
    }

    public function setMusescoreFile(?string $MusescoreFile): self
    {
        $this->musescoreFile = $MusescoreFile;

        return $this;
    }

    /**
     * @return Collection|Mp3[]
     */
    public function getMp3s(): Collection
    {
        return $this->mp3s;
    }

    public function addMp3(Mp3 $mp3): self
    {
        if (!$this->mp3s->contains($mp3)) {
            $this->mp3s[] = $mp3;
            $mp3->setPartitura($this);
        }

        return $this;
    }

    public function removeMp3(Mp3 $mp3): self
    {
        if ($this->mp3s->contains($mp3)) {
            $this->mp3s->removeElement($mp3);
            // set the owning side to null (unless already changed)
            if ($mp3->getPartitura() === $this) {
                $mp3->setPartitura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Links[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Links $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setPartitura($this);
        }

        return $this;
    }

    public function removeLink(Links $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            // set the owning side to null (unless already changed)
            if ($link->getPartitura() === $this) {
                $link->setPartitura(null);
            }
        }

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Collection|Comentario[]
     */
    public function getComentarios(): Collection
    {
        return $this->comentarios;
    }

    public function addComentario(Comentario $comentario): self
    {
        if (!$this->comentarios->contains($comentario)) {
            $this->comentarios[] = $comentario;
            $comentario->setPartitura($this);
        }

        return $this;
    }

    public function removeComentario(Comentario $comentario): self
    {
        if ($this->comentarios->contains($comentario)) {
            $this->comentarios->removeElement($comentario);
            // set the owning side to null (unless already changed)
            if ($comentario->getPartitura() === $this) {
                $comentario->setPartitura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PartituraPdf[]
     */
    public function getPartituraPdfs(): Collection
    {
        return $this->partituraPdfs;
    }

    public function addPartituraPdf(PartituraPdf $partituraPdf): self
    {
        if (!$this->partituraPdfs->contains($partituraPdf)) {
            $this->partituraPdfs[] = $partituraPdf;
            $partituraPdf->setPartitura($this);
        }

        return $this;
    }

    public function removePartituraPdf(PartituraPdf $partituraPdf): self
    {
        if ($this->partituraPdfs->contains($partituraPdf)) {
            $this->partituraPdfs->removeElement($partituraPdf);
            // set the owning side to null (unless already changed)
            if ($partituraPdf->getPartitura() === $this) {
                $partituraPdf->setPartitura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserFav[]
     */
    public function getUserFavs(): Collection
    {
        return $this->userFavs;
    }

    public function addUserFav(UserFav $userFav): self
    {
        if (!$this->userFavs->contains($userFav)) {
            $this->userFavs[] = $userFav;
            $userFav->setPartitura($this);
        }

        return $this;
    }

    public function removeUserFav(UserFav $userFav): self
    {
        if ($this->userFavs->contains($userFav)) {
            $this->userFavs->removeElement($userFav);
            // set the owning side to null (unless already changed)
            if ($userFav->getPartitura() === $this) {
                $userFav->setPartitura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserListPartitura>
     */
    public function getUserListPartituras(): Collection
    {
        return $this->userListPartituras;
    }

    public function addUserListPartitura(UserListPartitura $userListPartitura): self
    {
        if (!$this->userListPartituras->contains($userListPartitura)) {
            $this->userListPartituras[] = $userListPartitura;
            $userListPartitura->setPartitura($this);
        }

        return $this;
    }

    public function removeUserListPartitura(UserListPartitura $userListPartitura): self
    {
        if ($this->userListPartituras->removeElement($userListPartitura)) {
            // set the owning side to null (unless already changed)
            if ($userListPartitura->getPartitura() === $this) {
                $userListPartitura->setPartitura(null);
            }
        }

        return $this;
    }

}
