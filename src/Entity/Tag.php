<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 * @Vich\Uploadable
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @Assert\File(maxSize="1M")
     * @Vich\UploadableField(mapping="image", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Partitura", mappedBy="tags")
     */
    private $partituras;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TagText", mappedBy="tag")
     */
    private $tagTexts;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAuthor;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInstrument;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isOrigin;

    public function __construct()
    {
        $this->partituras = new ArrayCollection();
        $this->tagTexts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
    public function __toString()
    {
        return $this->name;
    }

    public function setName(string $Name): self
    {
        $this->name = $Name;

        return $this;
    }
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
    /**
     * @param File|UploadedFile $imageFile
     */
    public function setImagefile(?File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }
    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }


    /**
     * @return Collection|Partitura[]
     */
    public function getPartituras(): Collection
    {
        return $this->partituras;
    }

    public function addPartitura(Partitura $partitura): self
    {
        if (!$this->partituras->contains($partitura)) {
            $this->partituras[] = $partitura;
            $partitura->setType($this);
        }

        return $this;
    }

    public function removePartitura(Partitura $partitura): self
    {
        if ($this->partituras->contains($partitura)) {
            $this->partituras->removeElement($partitura);
            // set the owning side to null (unless already changed)
            if ($partitura->getType() === $this) {
                $partitura->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TagText[]
     */
    public function getTagTexts(): Collection
    {
        return $this->tagTexts;
    }

    public function addTagText(TagText $tagText): self
    {
        if (!$this->tagTexts->contains($tagText)) {
            $this->tagTexts[] = $tagText;
            $tagText->setTag($this);
        }

        return $this;
    }

    public function removeTagText(TagText $tagText): self
    {
        if ($this->tagTexts->contains($tagText)) {
            $this->tagTexts->removeElement($tagText);
            // set the owning side to null (unless already changed)
            if ($tagText->getTag() === $this) {
                $tagText->setTag(null);
            }
        }

        return $this;
    }

    public function getIsAuthor(): ?bool
    {
        return $this->isAuthor;
    }

    public function setIsAuthor(bool $isAuthor): self
    {
        $this->isAuthor = $isAuthor;

        return $this;
    }

    public function getIsType(): ?bool
    {
        return $this->isType;
    }

    public function setIsType(?bool $isType): self
    {
        $this->isType = $isType;

        return $this;
    }

    public function getIsInstrument(): ?bool
    {
        return $this->isInstrument;
    }

    public function setIsInstrument(bool $isInstrument): self
    {
        $this->isInstrument = $isInstrument;

        return $this;
    }
    public function getIsOrigin(): ?bool
    {
        return $this->isOrigin;
    }

    public function setIsOrigin(bool $isOrigin): self
    {
        $this->isOrigin = $isOrigin;

        return $this;
    }
    public function getType()
    {
        if($this->getIsAuthor()) { return 'author'; }
        if($this->getIsInstrument()) { return 'instrument'; }
        if($this->getIsOrigin()) { return 'origin'; }
        if($this->getIsType()) { return 'type'; }
        return 'type';
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
}
