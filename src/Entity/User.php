<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @UniqueEntity(fields={"Name"}, message="There is already an account with this name")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Partitura", mappedBy="user")
     */
    private $partituras;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserFav", mappedBy="User")
     */
    private $userFavs;

    /**
     * @ORM\OneToMany(targetEntity=UserList::class, mappedBy="user")
     */
    private $userLists;

    public function __construct()
    {
        $this->partituras = new ArrayCollection();
        $this->userFavs = new ArrayCollection();
        $this->userLists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }
    public function __toString()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Partitura[]
     */
    public function getPartituras(): Collection
    {
        return $this->partituras;
    }

    public function addPartitura(Partitura $partitura): self
    {
        if (!$this->partituras->contains($partitura)) {
            $this->partituras[] = $partitura;
            $partitura->setUser($this);
        }

        return $this;
    }

    public function removePartitura(Partitura $partitura): self
    {
        if ($this->partituras->contains($partitura)) {
            $this->partituras->removeElement($partitura);
            // set the owning side to null (unless already changed)
            if ($partitura->getUser() === $this) {
                $partitura->setUser(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|UserFav[]
     */
    public function getUserFavs(): Collection
    {
        return $this->userFavs;
    }

    /**
     * @return Collection|favpartituras[]
     */
    public function getFavPartituras(): Collection
    {
        $favPartituras = new ArrayCollection();
        foreach($this->userFavs as $fav)
        {
            $favPartituras[] = $fav->getPartitura();
        }
        return $favPartituras;
    }

    public function addUserFav(UserFav $userFav): self
    {
        if (!$this->userFavs->contains($userFav)) {
            $this->userFavs[] = $userFav;
            $userFav->setUser($this);
        }

        return $this;
    }

    public function removeUserFav(UserFav $userFav): self
    {
        if ($this->userFavs->contains($userFav)) {
            $this->userFavs->removeElement($userFav);
            // set the owning side to null (unless already changed)
            if ($userFav->getUser() === $this) {
                $userFav->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserList>
     */
    public function getUserLists(): Collection
    {
        return $this->userLists;
    }

    public function addUserList(UserList $userList): self
    {
        if (!$this->userLists->contains($userList)) {
            $this->userLists[] = $userList;
            $userList->setUser($this);
        }

        return $this;
    }

    public function removeUserList(UserList $userList): self
    {
        if ($this->userLists->removeElement($userList)) {
            // set the owning side to null (unless already changed)
            if ($userList->getUser() === $this) {
                $userList->setUser(null);
            }
        }

        return $this;
    }
}
