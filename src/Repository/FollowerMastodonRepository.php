<?php

namespace App\Repository;

use App\Entity\FollowerMastodon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;



/**
 * @method FollowerMastodon|null find($id, $lockMode = null, $lockVersion = null)
 * @method FollowerMastodon|null findOneBy(array $criteria, array $orderBy = null)
 * @method FollowerMastodon[]    findAll()
 * @method FollowerMastodon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FollowerMastodonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FollowerMastodon::class);
    }

    // /**
    //  * @return FollowerMastodon[] Returns an array of FollowerMastodon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FollowerMastodon
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
