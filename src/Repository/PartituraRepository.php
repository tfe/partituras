<?php

namespace App\Repository;

use App\Entity\Partitura;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Partitura|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partitura|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partitura[]    findAll()
 * @method Partitura[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartituraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partitura::class);
    }

    public function findAllMastodon($min_id=null, $max_id=null, $limit=20)
    {
        $builder = $this->createQueryBuilder('p')
            ->setMaxResults($limit)
            ->andWhere('p.visible = :visible')
            ->setParameter('visible', true);

        $reverse_result=false;
        if(preg_match("/^\d+$/",$max_id))
        {
            $builder
            ->andWhere('p.id < :id')
            ->setParameter('id', $max_id)
            ->addOrderBy('p.id', 'DESC');
        }
        elseif(preg_match("/^\d+$/",$min_id))
        {
            $reverse_result=true;
            $builder
            ->andWhere('p.id > :id')
            ->setParameter('id', $min_id)
            ->addOrderBy('p.id', 'ASC');
        }
        else
        {
            $builder->addOrderBy('p.id','DESC');
        }
        $query = $builder->getQuery();
        $result =  $query->getResult();
        if($reverse_result)
        {
            $result =array_reverse($result);
        }
        return $result;
    }

    public function findLatest($page=1, $max_results=5, $filter)
    {
        if (!is_numeric($page)) {
            throw new InvalidArgumentException("Invalid page number");
        }

        $builder = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->setFirstResult(($page-1)*$max_results)
            ->andWhere('p.visible = :visible')
            ->setParameter('visible', true);

        $col =$filter->getCol();
        if(!preg_match("/^(name|createdAt|views)$/", $col))
        {
            $col="createdAt";
        }
        if(!empty($filter->getSearch()))
        {
            $builder->andWhere(" p.name like :search or p.text like :search or p.description like :search ");
            $builder->setParameter("search", "%".$filter->getSearch()."%");
        }
        $sort ="desc";
        if($col=="name")
        {
            $sort="asc";
        }
        $builder->addOrderBy('p.'.$col, $sort);
        $query = $builder->getQuery();

        $paginator = new Paginator($query);
        return $paginator;
    }

    public function findToAdd($page=1, $max_results=5, $filter)
    {
        if (!is_numeric($page)) {
            throw new InvalidArgumentException("Invalid page number");
        }

        $builder = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->setFirstResult(($page-1)*$max_results)
        ;

        $col ="name";
        $builder->andWhere(" p.name like :search or p.text like :search or p.description like :search ");
        $builder->setParameter("search", "%".$filter->getSearch()."%");
        $sort="asc";
        $builder->addOrderBy('p.'.$col, $sort);
        $query = $builder->getQuery();

        return $query->getResult();
    }

    public function findByInstrument($instrument, $page=1, $max_results=18, $filter)
    {
        if (!is_numeric($page)) {
            throw new InvalidArgumentException("Invalid page number");
        }

        $col =$filter->getCol();
        if(!preg_match("/^(name|createdAt|views)$/", $col))
        {
            $col="createdAt";
        }
        $sort ="desc";
        if($col=="name")
        {
            $sort="asc";
        }

        $query = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->join('p.mp3s','mp3s')
            ->join('p.partituraPdfs','pdfs')
            ->setFirstResult(($page-1)*$max_results)
            ->andWhere('mp3s.instrumento = :instrument and p.visible=:visible')
            ->orWhere('pdfs.instrumento = :instrument and p.visible=:visible')
            ->setParameter('visible', true)
            ->setParameter('instrument', $instrument)
            ->addOrderBy('p.'.$col, $sort)
            ->getQuery();
        $paginator = new Paginator($query);
        return $paginator;
    }

    public function recommendations($tag, $partitura)
    {
        $result = [
        ];

        $next = $this->createQueryBuilder('p')
            ->setMaxResults(5)
            ->andWhere('p.visible = :visible and :tag member of p.tags')
            ->andWhere('p.id > :id')
            ->setParameter('visible', true)
            ->setParameter('tag', $tag)
            ->setParameter('id', $partitura->getId())
            ->setParameter('visible', true)
            ->addOrderBy('p.id','asc')
            ->getQuery()
            ->getResult();
        if($next) {
            $result = array_merge($result, $next);
        }
        $prev = $this->createQueryBuilder('p')
            ->setMaxResults(5)
            ->andWhere('p.visible = :visible and :tag member of p.tags')
            ->andWhere('p.id < :id')
            ->setParameter('visible', true)
            ->setParameter('tag', $tag)
            ->setParameter('id', $partitura->getId())
            ->setParameter('visible', true)
            ->addOrderBy('p.id','desc')
            ->getQuery()
            ->getResult();
        if($prev) {
            $result = array_merge($result, $prev);
        }
        return $result;

    }
    public function by_tag($tag, $page=1, $max_results=18, $filter)
    {
        if (!is_numeric($page)) {
            throw new InvalidArgumentException("Invalid page number");
        }

        $builder = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->setFirstResult(($page-1)*$max_results)
            ->andWhere('p.visible = :visible and :tag member of p.tags')
            ->setParameter('visible', true)
            ->setParameter('tag', $tag)
            ->setParameter('visible', true);

        $col =$filter->getCol();
        if(!preg_match("/^(name|createdAt|views)$/", $col))
        {
            $col="createdAt";
        }
        if(!empty($filter->getSearch()))
        {
            $builder->andWhere(" p.name like :search or p.text like :search or p.credito like :search or p.description like :search ");
            $builder->setParameter("search", "%".$filter->getSearch()."%");
        }
        $sort ="desc";
        if($col=="name")
        {
            $sort="asc";
        }
        $builder->addOrderBy('p.'.$col, $sort);
        $query = $builder->getQuery();

        $paginator = new Paginator($query);
        return $paginator;
    }


    public function search($name, $page=1,$max_results=18)
    {
        $query = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->setFirstResult(($page-1)*$max_results)


            ->andWhere('p.visible = :visible and p.name like :search')
            ->orWhere('p.visible = :visible and p.text like :search')

            ->setParameter('visible', true)
            ->setParameter('search', "%$name%")

            ->addOrderBy('p.name','asc')
            ->getQuery();
        $paginator = new Paginator($query);
        return $paginator;
    }
}
