<?php

namespace App\Repository;

use App\Entity\TagText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method TagText|null find($id, $lockMode = null, $lockVersion = null)
 * @method TagText|null findOneBy(array $criteria, array $orderBy = null)
 * @method TagText[]    findAll()
 * @method TagText[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TagText::class);
    }

    // /**
    //  * @return TagText[] Returns an array of TagText objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TagText
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
