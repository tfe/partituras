<?php

namespace App\Repository;

use App\Entity\Tag;
use App\Entity\Partitura;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Tag|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tag|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tag[]    findAll()
 * @method Tag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }


    public function popularTags($params)
    {
        $query = $this->createQueryBuilder('t')
            ->select('t as tag, count(p) as num')
            ->innerJoin('t.partituras', 'p')
            ->addOrderBy('t.name','asc');

        foreach($params as $key=>$value) {
            $query->andWhere("t.$key = '$value'");
        }
        $query->andWhere("t.id != 95");
        $query = $query->groupBy('t.id')
              ->orderBy('num','DESC')
              ->setMaxResults(10)
              ->getQuery()
              ->getResult();
        return $query;
    }
    public function search($name, $page=1,$max_results=18)
    {
        $query = $this->createQueryBuilder('p')
            ->setMaxResults($max_results)
            ->setFirstResult(($page-1)*$max_results)


            ->orWhere('p.name like :search')
            ->setParameter('search', "%$name%")

            ->addOrderBy('p.name','asc')
            ->getQuery()
            ->getResult();
        return $query;
    }
}
