<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Tag;
use Doctrine\ORM\EntityRepository;

class FilterType extends AbstractType
{
    private $translator;
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $cols = [
            $this->translator->trans('Upload Date') => 'createdAt',
            $this->translator->trans('Name of the Musicsheet') => 'name',
            $this->translator->trans('Views') => 'views',
        ];
        $tags = [
            $this->translator->trans('Asc') => 'asc',
            $this->translator->trans('Desc') => 'desc'
        ];

        $builder
            ->add('search', TextType::class, ['attr' => ['placeholder' => 'Search'], 'required' => false])
        /*
            ->add('tags', EntityType::class,
                [
                    'label' => 'Additional tags',
                    'class' => Tag::class,
                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                  ->orderBy('u.isOrigin, u.isAuthor, u.isType, u.name', 'ASC');
                    },
                    'choice_attr' => function($choice, $key, $value)
                    {
                        if($choice->getIsOrigin()) { return ['data-origin' => 1]; }
                        if($choice->getIsAuthor()) { return ['data-author' => 1]; }
                        if($choice->getIsType()) { return ['data-type' => 1]; }
                        if($choice->getIsInstrument()) { return ['data-instrument' => 1]; }
                        return ['data-other' => 1];
                    },
                    'group_by' =>  function($x)
                    {
                        if($x->getIsAuthor()) { return '4 - '.$this->translator->trans('Musicsheet author'); }
                        if($x->getIsOrigin()) { return '3 - '.$this->translator->trans('Musicsheet origin'); }
                        if($x->getIsType()) { return '2 - '.$this->translator->trans('Musicsheet type'); }
                        if($x->getIsInstrument()) { return '1 - '.$this->translator->trans('Musicsheet instruments'); }
                        return '5 - '.$this->translator->trans('Musicsheet others');
                    },
                    'choice_label' => 'name',
                    'choice_value' => 'name'
                ])
         */
            ->add('col', ChoiceType::class, ['choices' => $cols, 'label' => false, 'expanded' => false ])
            ->add('submit', SubmitType::class, [ 'label' => 'Search', 'attr' => ['name' => null] ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['prefix' => '', 'csrf_protection' => false ]);
    }
    public function getBlockPrefix()
    {
        return null;
    }
}
