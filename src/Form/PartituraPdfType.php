<?php

namespace App\Form;

use App\Entity\PartituraPdf;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\PreSetDataEvent;

class PartituraPdfType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (PreSetDataEvent $event) {
            $form = $event->getForm();
            $entity = $event->getData();
            $required = !$entity;

            $form
                ->add('name', null, ['label' => 'name'])
                ->add('instrumento',null, [ 'required' => true, 'label' =>'instrument'])
                ->add('pdfFile', VichFileType::class, [
                    'attr' => ['accept' => '.pdf, .jpg, .jpeg, .png'], 
                    'row_attr' => ['class' => $required ? '' : 'vich_filled'],
                    'required' => $required,
                    'label' => 'Pdf music sheet',
                    'allow_delete' => true
                ]);


        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'creation' =>  false,
            'csrf_protection' => false,
            'data_class' => PartituraPdf::class,
        ]);
    }
}
