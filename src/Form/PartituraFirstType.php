<?php

namespace App\Form;
use Acme\DemoBundle\Form\Type\FieldsetType;

use Doctrine\ORM\EntityRepository;
use App\Form\UserType as UserType;
use App\Form\PartituraPdfType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Partitura;
use App\Entity\Tag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Regex as Regex;
use App\Repository\TagRepository as TagRepository;


class PartituraFirstType extends AbstractType
{
    private $security;
    private $translator;
    public function __construct(Security $security, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('musescoreFileFile', VichFileType::class,
            [
                'label' => 'Pdf music sheet',
                'required' => $options['creation'], 
                'row_attr' => [
                    'class' => 'main_pdf'
                ],
                'attr' => ['accept' => '.pdf, .jpg, .jpeg, .png'], 
                'allow_delete' => true ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'creation' => false,
            'data_class' => Partitura::class,
        ]);
    }
}
