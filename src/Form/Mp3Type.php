<?php

namespace App\Form;

use App\Entity\Mp3;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\PreSetDataEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class Mp3Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (PreSetDataEvent $event) {
            $form = $event->getForm();
            $entity = $event->getData();
            $required = !$entity;

            $form
                ->add('instrumento',null, [ 'required' => true, 'label' => false])
                ->add('mp3File', VichFileType::class, [
                    'attr' => ['class' => ($required ? '' : ''),'accept' => '.mp3' ], 
                    'row_attr' => ['class' => $required ? '' : 'vich_filled'],
                    'required' => $required, 
                    'allow_delete' => true, 
                    'label' => false])
            ;

        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'creation' =>  false,
            'data_class' => Mp3::class,
        ]);
    }
}
