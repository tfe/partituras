# Code source of partiturak.eus page

This source allows you to create a web page for hosting musicsheets, with an embeded audio player.


## Features

* List of music sheets
* Browsable categories
* User accounts and user uploads
* Favorites Music sheets to find them easily later

## Missing / TODO

* Fix Safari audio
* Sortable lists
* 