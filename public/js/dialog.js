document.addEventListener('DOMContentLoaded', () => {
    let dialogs = document.querySelectorAll('dialog');

    document.querySelector('.partitura_list') &&
    document.querySelector('.partitura_list').addEventListener('click', () => {
        document.querySelector('.listable').classList.add('visible');
    });

    document.querySelector('.assign_list') &&
    document.querySelector('.assign_list').addEventListener('click', (e) => {
        let id = e.target.getAttribute('data-id');
        let action = e.target.getAttribute('data-action');

        e.stopPropagation();
        e.preventDefault();
        document.querySelector('.listable').classList.remove('visible');
        document.querySelectorAll('.listables input').forEach((input) =>
        {
            let list_id = input.getAttribute('data-list_id');
            let status = input.checked ? 1 : 0;

            fetch(action.replace('_partitura_',id).replace('_list_', list_id).replace('_status_',status))
            console.log('UPDATE : ',list_id, status);
        });
    });
    dialogs.forEach(function(dialog)
        {
            dialog.querySelector('.dialog_content').addEventListener('click', function(e)
            {
                e.stopPropagation();
            });

            dialog.addEventListener('click', function(e)
            {
                dialog.classList.remove('visible');
            });
        });
    window.setTimeout(function() { document.querySelector('.partitura_tabs-links a.selected').click();}, 100);
});
