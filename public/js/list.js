
if(document.querySelectorAll('.partitura_filters select')) {
    document.querySelectorAll('.partitura_filters select').forEach(function(item) {
        item.addEventListener('change', (e) => 
        {
            window.setTimeout(function()
            {
                let s = new Event('submit');
                document.querySelector('.partitura_filters form').dispatchEvent(s);
            }, 100);
        });
    });
}

if(document.querySelectorAll('button.partitura_star')) {
    document.querySelectorAll('button.partitura_star').forEach(function(item) {
        item.addEventListener('click', (e) => 
        {
            e.stopPropagation();
            fetch(item.getAttribute('data-href')).then((j) => 
            {
                return j.json();
            }).then((j) => {
                item.classList.toggle('fav', j.result);
                let num = parseInt(e.target.innerHTML,10);
                if(!isNaN(num))
                {
                    num += item.classList.contains('fav') ? 1 : -1;
                    item.innerHTML=num;
                }
            });
        }, true);
    });
}


