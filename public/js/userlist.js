
if(document.querySelector('.btn_open'))
{
    document.querySelector('.btn_open').addEventListener('click', (e) => {
        document.querySelector('#search').value='';
        document.querySelector('.list_container').innerHTML='';
        document.querySelector('.list_add').classList.add('opened');
        document.querySelector('#search').focus();
    });

    document.querySelector('.btn_close').addEventListener('click', (e) => {
        document.querySelector('.list_add').classList.remove('opened');
    });
    document.querySelector('#search').setAttribute('autocomplete', 'off')

    async function searchForm()
    {
        let form = document.querySelector('.partitura_filters form');
        let action = form.action;
        let input = document.querySelector('#search').value;

        let r = await fetch(form.action+'?json&search='+encodeURIComponent(input));
        let j = await r.json();

        let html ='';
        if(j.partituras)
        {
            j.partituras.forEach((partitura) => {
                let item = `<section class="partitura_item">
                            <div class="partitura_header">
                                <p class="partitura_name">
                                    <a href="${partitura.add_link}">
                                        ${partitura.name}
                                    </a>
                                </p>
                            </div>
                            <p class="partitura_image" style="--image:url(/${partitura.mainPdf.replace('.pdf','.jpg')})"></p>
                            </section>`;
                html+=item;

            });
        }
        document.querySelector('.list_container').innerHTML=html;
        console.log('here!',form.action, j);
    }



    new Sortable(document.querySelector('.partitura_filters .list_container'), {
            animation: 150,
            handle: '.partitura_move',

            onEnd: async function()
            {
                let ids = [];
                document.querySelectorAll('.partitura_filters .partitura_item').forEach((partitura) =>{
                    ids.push(partitura.getAttribute('data-listid'));
                });
                console.log('ok',ids);
                console.log('fetch ',reorder_link);
                let x = await fetch(reorder_link, {
                    method: 'POST',
                     headers: {
                               "Content-Type": "application/json",
                     },
                    body: JSON.stringify({ ids: ids})
                });
                let j = await x.json();
                console.log('end',ids, j);
            }
    });
    console.log('sortable ! ',document.querySelector('.partitura_filters .list_container'));



    document.querySelector('.partitura_filters form').addEventListener('submit', async (e) => {
        e.stopPropagation();
        e.preventDefault();
        searchForm();
    });

    let debounceTimer = null;
    document.querySelector('#search').addEventListener('keyup', async (e) => {
        window.clearTimeout(debounceTimer);
        e.stopPropagation();
        e.preventDefault();
        debounceTimer = window.setTimeout(searchForm, 250);
    });
}
