document.addEventListener('DOMContentLoaded', () => {


    $('.list_pdfs').on('click', '.pdf_remove', function()
    {
        let ref = $(this).closest('.pdf_item').attr('data-ref');
        console.log('removing is',ref);
        $('.pdf_tab_'+ref+', .pdf_'+ref).remove();
        $('.partitura_tabs-links a:eq(0)').get(0).click();
        return false;
    });

    $('.partitura_tabs-links').on('click', '.pdf_tab a', function()
    {
        let ref = $(this).closest('.pdf_tab').attr('data-ref');
        $('.pdf_tab a').removeClass('selected');
        $(this).addClass('selected');

        if(ref=='new')
        {
            let new_ref = parseInt($('.pdf_item').last().attr('data-ref'),10)+1;
            console.log('new ref is',new_ref);
            // Create new pdf_item
            let html = $('form.template_pdf').html();
            html = html.replace(/name="partitura_pdf/g, 'name="partitura[partiturapdfs]['+new_ref+']');
            html = html.replace(/id="partitura_pdf/g, 'id="partitura_partiturapdfs_'+new_ref);
            html = html.replace(/for="partitura_pdf/g, 'for="partitura_partiturapdfs_'+new_ref);

            let new_item = $('<div class="pdf_item pdf_'+new_ref+'" data-ref="'+new_ref+'">'+html+'<button class="btn btn-delete pdf_remove"></button></div>');
            new_item.find('.template_pdf').removeClass('template_pdf').addClass('sf_collection_item');
            $('.list_pdfs').append(new_item);
            ref=new_ref;

            $('.pdf_tab a').removeClass('selected');
            let name = $('.pdf_tab').length - 1+'. ahotsa';
            let new_tab = $('<li class="pdf_tab tab_'+new_ref+'" data-ref="'+new_ref+'"><a href="#" class="selected">'+name+'</a></li>');
            new_tab.insertBefore($('.pdf_tab[data-ref="new"]'));

            new_item.find('#partitura_partiturapdfs_'+new_ref+'_name').val(name);
            let file = new_item.find('input[type="file"]');
            file.click();
            let iframe = document.querySelector('.partitura_iframe iframe');
            iframe.setAttribute('src','about:blank');
        }
        else
        {
            let iframe = document.querySelector('.partitura_iframe iframe');
            let src = iframe.getAttribute('data-src');
            iframe.setAttribute('src',pdf_viewer+$(this).attr('href'));
        }

        // select correct tab content
        $('.list_pdfs .pdf_item').addClass('hidden');
        $('.pdf_'+ref).removeClass('hidden');


        console.log('SHOW',ref);

        return false;
    });
    //window.setTimeout(function() { document.querySelector('.partitura_tabs-links a.selected').click();}, 100);
});
