document.addEventListener('DOMContentLoaded', function()
    {
    document.querySelectorAll('a.embed').forEach((link) => 
    {
        link.addEventListener('click', function(e)
            {
                let link = e.target;
                let href  = link.href;
                if(href.match(/youtube.com\/embed/))
                {
                    href += '?autoplay=1';
                }
                openPopin(href);
                e.preventDefault();
                return false;
            }, false);
    });
    function openPopin(url)
    {
        var div = document.createElement('div');
        div.classList.add('popin');
        document.body.appendChild(div);

        div.innerHTML= `
        <div class="iframe_container">
            <div class="iframe">
            <button class="popin_close" aria-label="Close">X</button>
            <iframe src="${url}"></iframe>
            </div>
        </div>
        `;

        document.querySelector('.popin').addEventListener('click', function()
            {
                document.body.removeChild(document.querySelector('.popin'));
            });

    }
});


