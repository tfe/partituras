document.querySelectorAll('.button.partitura_star').forEach(function(item) {
    item.addEventListener('click', (e) => 
    {
        e.stopPropagation();
        fetch(item.getAttribute('data-href')).then((j) => 
        {
            return j.json();
        }).then((j) => {
            item.classList.toggle('fav', j.result);
        });
    }, true);
});
