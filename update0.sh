#!/bin/bash


SQL="mysql -N -hsql -uroot -proot partituras"


while read DATA
do
	ID=$(echo $DATA|cut -d'|' -f 1)
	AUTHOR=$(echo $DATA|cut -d'|' -f 2)
	TYPE=$(echo $DATA|cut -d'|' -f 3)
	echo "OK $ID / $AUTHOR / $TYPE"


	RESULT=$(echo "select CONCAT(partitura_id,'|',tag_id) from partitura_tag where partitura_id=$ID and tag_id=$AUTHOR" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $AUTHOR"
		echo "insert into partitura_tag (partitura_id, tag_id) VALUES($ID, $AUTHOR)" | $SQL
	else
		echo "ALREADY THERE $AUTHOR"
	fi

	# Check if the tag already exists or not
	RESULT=$(echo "select CONCAT(partitura_id,'|',tag_id) from partitura_tag where partitura_id=$ID and tag_id=$TYPE" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $TYPE"
		echo "insert into partitura_tag (partitura_id, tag_id) VALUES($ID, $TYPE)" | $SQL
	else
		echo "ALREADY THERE $TYPE"
	fi

done <<< $(echo "select CONCAT(id,'|',type_id,'|', author_id) from partitura " | $SQL)


# Check if the tag already exists or not
php bin/console doctrine:schema:update --force
echo "update partitura set url=name" | $SQL

