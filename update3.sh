#!/bin/bash

SQL="mysql -N -hsql -uroot -proot partituras"

echo "Update instrument tags "
while read DATA
do
	PARTITURA_ID=$(echo $DATA|cut -d'|' -f 1)
	NAME=$(echo $DATA|cut -d'|' -f 2)
	echo "OK $PARTITURA_ID / $NAME"


	RESULT=$(echo "select CONCAT(id,'|') from tag where name='$NAME' and is_origin=1" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $NAME"
		echo "insert into tag (name, is_author, is_type, is_instrument, is_origin) VALUES('$NAME', 0,0,0,1)" | $SQL
	else
		echo "ALREADY THERE $NAME"
	fi

	RESULT=$(echo "select CONCAT(id,'|') from tag where name='$NAME' and is_origin=1" | $SQL)
	TAGID=$(echo $RESULT|cut -d'|' -f 1)
	echo "$NAME = $TAGID"

	RESULT=$(echo "select * from partitura_tag where partitura_id='$PARTITURA_ID' and tag_id=$TAGID" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $PARTITURA_ID / $TAGID"
		echo "insert into partitura_tag (partitura_id , tag_id) VALUES($PARTITURA_ID, $TAGID)" | $SQL
	else
		echo "ALREADY THERE $PARTITURA_ID / $TAGID"
	fi

done <<< $(echo "select CONCAT(id,'|',credito,'|') from partitura where credito is not null  " | $SQL)

