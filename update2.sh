#!/bin/bash

SQL="mysql -N -hsql -uroot -proot partituras"

echo "Update instrument tags "
while read DATA
do
	ID=$(echo $DATA|cut -d'|' -f 1)
	NAME=$(echo $DATA|cut -d'|' -f 2)
	echo "OK $ID / $NAME"


	RESULT=$(echo "select CONCAT(id,'|') from tag where name='$NAME' and is_instrument=1" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $NAME"
		echo "insert into tag (name, is_author, is_origin, is_type, is_instrument) VALUES('$NAME', 0,0,0,1)" | $SQL
	else
		echo "ALREADY THERE $NAME"
	fi

	RESULT=$(echo "select CONCAT(id,'|') from tag where name='$NAME' and is_instrument=1" | $SQL)
	TAGID=$(echo $RESULT|cut -d'|' -f 1)
	echo "$NAME = $TAGID"

	RESULT=$(echo "update instrumento set  tag_id=$TAGID where id='$ID'" | $SQL)
	echo "Updated instrument... $RESULT"

done <<< $(echo "select CONCAT(id,'|',name,'|') from instrumento " | $SQL)

echo "Update mp3 tags"
while read DATA
do
	PARTITURA_ID=$(echo $DATA|cut -d'|' -f 1)
	TAG_ID=$(echo $DATA|cut -d'|' -f 2)
	echo "OK $PARTITURA_ID / $TAG_ID"


	RESULT=$(echo "select * from partitura_tag where partitura_id='$PARTITURA_ID' and tag_id=$TAG_ID" | $SQL)
	if [[ "$RESULT" == "" ]]
	then
		echo 	"adding $PARTITURA_ID / $TAG_ID"
		echo "insert into partitura_tag (partitura_id , tag_id) VALUES($PARTITURA_ID, $TAG_ID)" | $SQL
	else
		echo "ALREADY THERE $PARTITURA_ID / $TAG_ID"
	fi


done <<< $(echo "select CONCAT(m.partitura_id ,'|', i.tag_id)  from mp3 m left join instrumento i ON i.id =m.instrumento_id" | $SQL)





